import json
import pickle
import numpy as np
import tensorflow as tf


class Model(object):
    def __init__(self):
        # print('Init Example model...')
        self.idx2char = pickle_load('unicodes.p')
        self.img_size = (224, 224, 3)
        self.n_classes = len(self.idx2char)

        self.vgg = Vgg16(self.img_size, self.n_classes, 1)

        self.sess = tf.Session(config=get_tf_config())
        self.saver = tf.train.Saver()

        self.saver.restore(self.sess, 'weights/model-1')

    # def run(self, image):
    #     pred = self.sess.run(self.vgg.prob, feed_dict={self.vgg.images: image})
    #     pred = pred[0, :]
    #     idx = np.argmax(pred)
    #     uni = '\\u' + self.idx2char[idx]
    #     return uni.encode('ascii').decode('unicode-escape')

    def run(self, images):
        images = np.array(images)
        ch_seq = ''
        for image in images:
            if len(image.shape) == 3:
                image = image[np.newaxis, :, :, :]
            pred = self.sess.run(self.vgg.prob, feed_dict={self.vgg.images: image})
            pred = pred[0, :]
            idx = np.argmax(pred)
            uni = '\\u' + self.idx2char[idx]
            ch = uni.encode('ascii').decode('unicode-escape')
            ch_seq += ch
        result = {'chinese': ch_seq}
        return json.dumps(result)


class Vgg16(object):
    def __init__(self, img_size, n_classes, batch_size):
        # n_classes=2 in case of ECG signal, positive & negative
        self.img_size = (img_size[0], img_size[1], img_size[2])
        self.n_classes = n_classes
        self.batch_size = batch_size
        self.sen_coe_initialvalue = 30
        self.rob_coe_initialvalue = 9
        self.lamda = 0.5
        self.pool_range = 3
        self._convolution_model()

    @staticmethod
    def sort_pool2d(x, padding='SAME', k=1, name='sort_pool2d'):
        if k == 1:
            return tf.nn.max_pool(x, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding=padding, name=name)

        batch_size, height, width, num_channels = x.get_shape().as_list()
        pad_bottom = height % 2
        pad_right = width % 2
        #        height_div2 = height + pad_bottom
        #        width_div2 = width + pad_right
        if padding == 'SAME':
            x = tf.pad(x, [[0, 0], [0, pad_bottom], [0, pad_right], [0, 0]], "CONSTANT")

        _, height, width, _ = x.get_shape().as_list()
        offsets_y = tf.range(0, height, 2)
        offsets_x = tf.range(0, width, 2)

        sub_y0 = tf.gather(x, offsets_y, axis=1)
        sub_y1 = tf.gather(x, offsets_y + 1, axis=1)

        sub_00 = tf.gather(sub_y0, offsets_x, axis=2)
        sub_00 = tf.reshape(sub_00, [-1])
        sub_01 = tf.gather(sub_y0, offsets_x + 1, axis=2)
        sub_01 = tf.reshape(sub_01, [-1])

        sub_10 = tf.gather(sub_y1, offsets_x, axis=2)
        sub_10 = tf.reshape(sub_10, [-1])
        sub_11 = tf.gather(sub_y1, offsets_x + 1, axis=2)
        sub_11 = tf.reshape(sub_11, [-1])

        sub0 = tf.where(tf.less(sub_00, sub_01), tf.stack([sub_00, sub_01], axis=1),
                        tf.stack([sub_01, sub_00], axis=1))
        sub1 = tf.where(tf.less(sub_10, sub_11), tf.stack([sub_10, sub_11], axis=1),
                        tf.stack([sub_11, sub_10], axis=1))

        sub00 = tf.squeeze(tf.slice(sub0, [0, 0], [-1, 1]))
        sub01 = tf.squeeze(tf.slice(sub0, [0, 1], [-1, 1]))

        sub10 = tf.squeeze(tf.slice(sub1, [0, 0], [-1, 1]))
        sub11 = tf.squeeze(tf.slice(sub1, [0, 1], [-1, 1]))

        # assume elem1 <= elem3
        def sort_elems(elem1, elem2, elem3, elem4):
            elem2_less_than_elem3 = tf.stack([elem1, elem2, elem3, elem4], axis=1)
            elem2_greater_equal_elem3_and_elem2_less_than_elem4 = tf.stack([elem1, elem3, elem2, elem4], axis=1)
            elem2_greater_equal_elem3_and_elem2_greater_equal_elem4 = tf.stack([elem1, elem3, elem4, elem2],
                                                                               axis=1)
            elem2_greater_equal_elem3 = tf.where(tf.less(elem2, elem4),
                                                 elem2_greater_equal_elem3_and_elem2_less_than_elem4,
                                                 elem2_greater_equal_elem3_and_elem2_greater_equal_elem4)
            return tf.where(tf.less(elem2, elem3), elem2_less_than_elem3, elem2_greater_equal_elem3)

        sub00_less_sub10 = sort_elems(sub00, sub01, sub10, sub11)
        sub00_greater_equal_sub10 = sort_elems(sub10, sub11, sub00, sub01)

        sorted_sub_flat = tf.where(tf.less(sub00, sub10), sub00_less_sub10, sub00_greater_equal_sub10)
        sorted_sub = tf.slice(sorted_sub_flat, [0, 4 - k], [-1, k])
        sorted_sub = tf.reshape(sorted_sub, [-1, int(height / 2), int(width / 2), num_channels, k])

        #        sorted_sub = sorted_sub

        #        with tf.variable_scope(name):
        #          pool_weights = tf.get_variable('pool_weights', [1,1,1,1,k],
        #              tf.float32, initializer=tf.random_normal_initializer(stddev=0.1))
        #        pool_weights = tf.nn.softmax(pool_weights)
        #
        #        weighted_subsets = pool_weights*sorted_sub
        #        x = tf.reduce_sum(weighted_subsets, 4)
        return sorted_sub

    def _convolution_model(self):
        self.images = tf.placeholder(tf.float32, [self.batch_size,
                                                  self.img_size[0], self.img_size[1], self.img_size[2]])
        self.targets = tf.placeholder(tf.int32, [self.batch_size, 1])
        self.fdot_prev = tf.placeholder(tf.float32, [self.batch_size, self.n_classes])
        self.flag1 = tf.placeholder(tf.float32, [1])
        self.flag2 = tf.placeholder(tf.float32, [1])

        # conv1_1 ####################################
        kernel = tf.Variable(tf.random_normal(dtype=tf.float32, shape=[3, 3, self.img_size[2], 64]),
                             trainable=True, name='conv1_1_weights')
        conv = tf.nn.conv2d(self.images, kernel, [1, 1, 1, 1], padding='SAME')
        biases = tf.Variable(tf.random_normal(dtype=tf.float32, shape=[64]),
                             trainable=True, name='conv_1_1_biases')
        out = tf.nn.bias_add(conv, biases)
        out_bn = tf.layers.batch_normalization(inputs=out, axis=-1, momentum=0.9, epsilon=0.001, center=True,
                                               scale=True, training=True, name='conv1_1_bn')
        self.activations1 = tf.nn.relu(out_bn)

        # conv1_2 ####################################
        kernel = tf.Variable(tf.random_normal(dtype=tf.float32, shape=[3, 3, 64, 64]),
                             trainable=True, name='conv1_2_weights')
        conv = tf.nn.conv2d(self.activations1, kernel, [1, 1, 1, 1], padding='SAME')
        biases = tf.Variable(tf.random_normal(dtype=tf.float32, shape=[64]),
                             trainable=True, name='conv1_2_biases')
        out = tf.nn.bias_add(conv, biases)
        out_bn = tf.layers.batch_normalization(inputs=out, axis=-1, momentum=0.9, epsilon=0.001, center=True,
                                               scale=True, training=True, name='conv1_2_bn')
        self.activations2 = tf.nn.relu(out_bn)

        # pool1 ######################################
        # self.pker1 = tf.nn.softmax(tf.Variable(tf.constant(1, dtype=tf.float32, shape=[224, 224, 64]),
        #                                        trainable=True, name='poolKer1'))

        out = self.sort_pool2d(self.activations2, k=self.pool_range, name='pool1')
        self.pw1 = tf.nn.softmax(tf.Variable(tf.truncated_normal([1, 1, 1, 1, self.pool_range], stddev=0.1),
                                             name='pool_weights1'))
        weighted_pool = self.pw1*out
        self.pool1 = tf.reduce_sum(weighted_pool, 4)

        # conv2_1 ###################################
        kernel = tf.Variable(tf.random_normal(dtype=tf.float32, shape=[3, 3, 64, 128]),
                             trainable=True, name='conv2_1_weights')
        conv = tf.nn.conv2d(self.pool1, kernel, [1, 1, 1, 1], padding='SAME')
        biases = tf.Variable(tf.random_normal(dtype=tf.float32, shape=[128]),
                             trainable=True, name='conv2_1_biases')
        out = tf.nn.bias_add(conv, biases)
        out_bn = tf.layers.batch_normalization(inputs=out, axis=-1, momentum=0.9, epsilon=0.001,
                                               center=True, scale=True, training=True, name='conv2_1_bn')
        self.activations3 = tf.nn.relu(out_bn)

        # conv2_2 ##################################
        kernel = tf.Variable(tf.random_normal(dtype=tf.float32, shape=[3, 3, 128, 128]),
                             trainable=True, name='conv2_2_weights')
        conv = tf.nn.conv2d(self.activations3, kernel, [1, 1, 1, 1], padding='SAME')
        biases = tf.Variable(tf.random_normal(dtype=tf.float32, shape=[128]),
                             trainable=True, name='conv2_2_biases')
        out = tf.nn.bias_add(conv, biases)
        out_bn = tf.layers.batch_normalization(inputs=out, axis=-1, momentum=0.9, epsilon=0.001,
                                               center=True, scale=True, training=True, name='conv2_2_bn')
        self.activations4 = tf.nn.relu(out_bn)

        # pool2 #####################################
        # self.pker2 = tf.Variable(tf.constant(1,dtype = tf.float32, shape=[112,112,128]),
        #                          trainable=True, name='poolKer2')
        # poolker = tf.multiply(self.activations4, self.pker2)
        #
        # self.pool2 = self.sort_pool2d(poolker,
        #                               ksize=[1, 2, 2, 1],
        #                               strides=[1, 2, 2, 1],
        #                               padding='SAME',
        #                               name='pool2')

        out = self.sort_pool2d(self.activations4, k=self.pool_range, name='pool2')
        self.pw2 = tf.nn.softmax(tf.Variable(tf.truncated_normal([1, 1, 1, 1, self.pool_range], stddev=0.1),
                                             name='pool_weights2'))
        weighted_pool = self.pw2*out
        self.pool2 = tf.reduce_sum(weighted_pool, 4)

        # conv3_1 ###################################
        kernel = tf.Variable(tf.random_normal(dtype=tf.float32, shape=[3, 3, 128, 256]),
                             trainable=True, name='conv3_1_weights')
        conv = tf.nn.conv2d(self.pool2, kernel, [1, 1, 1, 1], padding='SAME')
        biases = tf.Variable(tf.random_normal(dtype=tf.float32, shape=[256]),
                             trainable=True, name='conv3_1_biases')
        out = tf.nn.bias_add(conv, biases)
        out_bn = tf.layers.batch_normalization(inputs=out, axis=-1, momentum=0.9, epsilon=0.001,
                                               center=True, scale=True, training=True, name='conv3_1_bn')
        self.activations5 = tf.nn.relu(out_bn)

        # conv3_2 ##################################
        kernel = tf.Variable(tf.random_normal(dtype=tf.float32, shape=[3, 3, 256, 256]),
                             trainable=True, name='conv3_2_weights')
        conv = tf.nn.conv2d(self.activations5, kernel, [1, 1, 1, 1], padding='SAME')
        biases = tf.Variable(tf.random_normal(dtype=tf.float32, shape=[256]),
                             trainable=True, name='conv3_2_biases')
        out = tf.nn.bias_add(conv, biases)
        out_bn = tf.layers.batch_normalization(inputs=out, axis=-1, momentum=0.9, epsilon=0.001,
                                               center=True, scale=True, training=True, name='conv3_2_bn')
        self.activations6 = tf.nn.relu(out_bn)

        # conv3_3 #################################
        kernel = tf.Variable(tf.random_normal(dtype=tf.float32, shape=[3, 3, 256, 256]),
                             trainable=True, name='conv3_3_weights')
        conv = tf.nn.conv2d(self.activations6, kernel, [1, 1, 1, 1], padding='SAME')
        biases = tf.Variable(tf.random_normal(dtype=tf.float32, shape=[256]),
                             trainable=True, name='conv3_3_biases')
        out = tf.nn.bias_add(conv, biases)
        out_bn = tf.layers.batch_normalization(inputs=out, axis=-1, momentum=0.9, epsilon=0.001,
                                               center=True, scale=True, training=True, name='conv3_3_bn')
        self.activations7 = tf.nn.relu(out_bn)

        # pool3 ###################################
        # self.pker3 = tf.Variable(tf.constant(1,dtype = tf.float32, shape=[56,56,256]),
        #                          trainable=True, name='poolKer3')
        out = self.sort_pool2d(self.activations7, k=self.pool_range, name='pool3')
        self.pw3 = tf.nn.softmax(tf.Variable(tf.truncated_normal([1, 1, 1, 1, self.pool_range], stddev=0.1),
                                             name='pool_weights3'))
        weighted_pool = self.pw3*out
        self.pool3 = tf.reduce_sum(weighted_pool, 4)

        # conv4_1 #################################
        kernel = tf.Variable(tf.random_normal(dtype=tf.float32, shape=[3, 3, 256, 512]),
                             trainable=True, name='conv4_1_weights')
        conv = tf.nn.conv2d(self.pool3, kernel, [1, 1, 1, 1], padding='SAME')
        biases = tf.Variable(tf.random_normal(dtype=tf.float32, shape=[512]),
                             trainable=True, name='conv4_1_biases')
        out = tf.nn.bias_add(conv, biases)
        out_bn = tf.layers.batch_normalization(inputs=out, axis=-1, momentum=0.9, epsilon=0.001,
                                               center=True, scale=True, training=True, name='conv4_1_bn')
        self.activations8 = tf.nn.relu(out_bn)

        # conv4_2 ################################
        kernel = tf.Variable(tf.random_normal(dtype=tf.float32, shape=[3, 3, 512, 512]),
                             trainable=True, name='conv4_2_weights')
        conv = tf.nn.conv2d(self.activations8, kernel, [1, 1, 1, 1], padding='SAME')
        biases = tf.Variable(tf.random_normal(dtype=tf.float32, shape=[512]),
                             trainable=True, name='conv4_2_biases')
        out = tf.nn.bias_add(conv, biases)
        out_bn = tf.layers.batch_normalization(inputs=out, axis=-1, momentum=0.9, epsilon=0.001,
                                               center=True, scale=True, training=True, name='conv4_2_bn')
        self.activations9 = tf.nn.relu(out_bn)

        # conv4_3 ################################
        kernel = tf.Variable(tf.random_normal(dtype=tf.float32, shape=[3, 3, 512, 512]),
                             trainable=True, name='conv4_3_weights')
        conv = tf.nn.conv2d(self.activations9, kernel, [1, 1, 1, 1], padding='SAME')
        biases = tf.Variable(tf.random_normal(dtype=tf.float32, shape=[512]),
                             trainable=True, name='conv4_3_biases')
        out = tf.nn.bias_add(conv, biases)
        out_bn = tf.layers.batch_normalization(inputs=out, axis=-1, momentum=0.9, epsilon=0.001, center=True,
                                               scale=True, training=True, name='conv4_3_bn')
        self.activations10 = tf.nn.relu(out_bn)

        # pool4 #################################
        # self.pker4 = tf.Variable(tf.constant(1,dtype = tf.float32, shape=[28,28,512]),
        #                          trainable=True, name='poolKer4')
        out = self.sort_pool2d(self.activations10, k=self.pool_range, name='pool4')
        self.pw4 = tf.nn.softmax(tf.Variable(tf.truncated_normal([1, 1, 1, 1, self.pool_range], stddev=0.1),
                                             name='pool_weights4'))
        weighted_pool = self.pw4*out
        self.pool4 = tf.reduce_sum(weighted_pool, 4)

        # conv5_1 ###############################
        kernel = tf.Variable(tf.random_normal(dtype=tf.float32, shape=[3, 3, 512, 512]),
                             trainable=True, name='conv5_1_weights')
        conv = tf.nn.conv2d(self.pool4, kernel, [1, 1, 1, 1], padding='SAME')
        biases = tf.Variable(tf.random_normal(dtype=tf.float32, shape=[512]),
                             trainable=True, name='conv5_1_biases')
        out = tf.nn.bias_add(conv, biases)
        out_bn = tf.layers.batch_normalization(inputs=out, axis=-1, momentum=0.9, epsilon=0.001,
                                               center=True, scale=True, training=True, name='conv5_1_bn')
        self.activations11 = tf.nn.relu(out_bn)

        # conv5_2 ###############################
        kernel = tf.Variable(tf.random_normal(dtype=tf.float32, shape=[3, 3, 512, 512]),
                             trainable=True, name='conv5_2_weights')
        conv = tf.nn.conv2d(self.activations11, kernel, [1, 1, 1, 1], padding='SAME')
        biases = tf.Variable(tf.random_normal(dtype=tf.float32, shape=[512]),
                             trainable=True, name='conv5_2_biases')
        out = tf.nn.bias_add(conv, biases)
        out_bn = tf.layers.batch_normalization(inputs=out, axis=-1, momentum=0.9, epsilon=0.001,
                                               center=True, scale=True, training=True, name='conv5_2_bn')
        self.activations12 = tf.nn.relu(out_bn)

        # conv5_3 ###############################
        kernel = tf.Variable(tf.random_normal(dtype=tf.float32, shape=[3, 3, 512, 512]),
                             trainable=True, name='conv5_3_weights')
        conv = tf.nn.conv2d(self.activations12, kernel, [1, 1, 1, 1], padding='SAME')
        biases = tf.Variable(tf.random_normal(dtype=tf.float32, shape=[512]),
                             trainable=True, name='conv5_3_biases')
        out = tf.nn.bias_add(conv, biases)
        out_bn = tf.layers.batch_normalization(inputs=out, axis=-1, momentum=0.9, epsilon=0.001,
                                               center=True, scale=True, training=True, name='conv5_3_bn')
        self.activations13 = tf.nn.relu(out_bn)

        # pool5 #################################
        # self.pker5 = tf.Variable(tf.constant(1,dtype = tf.float32, shape=[14,14,512]),
        #                          trainable=True, name='poolKer5')
        out = self.sort_pool2d(self.activations13, k=self.pool_range, name='pool5')
        self.pw5 = tf.nn.softmax(tf.Variable(tf.truncated_normal([1, 1, 1, 1, self.pool_range], stddev=0.1),
                                             name='pool_weights5'))
        weighted_pool = self.pw5*out
        self.pool5 = tf.reduce_sum(weighted_pool, 4)
        self.GAP = tf.nn.max_pool(self.pool5, ksize=[1, 7, 7, 1], strides=[1, 7, 7, 1], padding='SAME', name='GAP')
        shape = int(np.prod(self.GAP.get_shape()[1:]))
        self.GAP_flatten = tf.reshape(self.GAP, [-1, shape])
        self.GAP_dropout = tf.nn.dropout(self.GAP_flatten, 0.75)

        # fc1 ##################################
        fc3w = tf.Variable(tf.truncated_normal([512, self.n_classes], stddev=0.1), name='fc3_weights')
        fc3b = tf.Variable(tf.constant(0.1, shape=[self.n_classes]), name='fc3_biases')
        self.fc3l = tf.nn.bias_add(tf.matmul(self.GAP_dropout, fc3w), fc3b)
        # No activation function (ReLU-softplus) is used for last layer
        self.prob = tf.nn.softmax(self.fc3l)


def pickle_store(obj, path):
    f = open(path, 'wb')
    pickle.dump(obj, f)
    f.close()
    return


def pickle_load(path):
    f = open(path, 'rb')
    a = pickle.load(f)
    f.close()
    return a


def get_tf_config():
    config = tf.ConfigProto(allow_soft_placement=True)
    # config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    return config


# if __name__ == '__main__':
#     with open('test.json') as j:
#         data = json.loads(json.load(j))
#
#     # imgs = np.array(data['검출결과크롭'])
#     # img = img[np.newaxis, :, :, :]
#     # print(len(data['검출결과크롭']))
#     # print(type(data['검출결과크롭']))
#     model = Model()
#     print(model.run(data['검출결과크롭']))
