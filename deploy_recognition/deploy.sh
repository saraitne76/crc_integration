#!/usr/bin/env bash
nvidia-docker stop recognition_container
nvidia-docker rm recognition_container
nvidia-docker build --network host -t recognition_image .
nvidia-docker run -d -p 50052:50052 --name recognition_container recognition_image