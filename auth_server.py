from flask import Flask, jsonify
import jwt
import datetime

app = Flask(__name__)
secret_key = 'secret_key'

@app.route('/')
def index():
    return "Hello auth server" # Test for server is working

@app.route("/get_token")
def get_token():
    issuer = 'abr'
    subject = 'localhost:50000/v1'
    date_time_obj = datetime.datetime
    exp_time = date_time_obj.timestamp(date_time_obj.utcnow() + datetime.timedelta(hours=24))
    scope = ['v1', 'v2']
    payload = {
        'sub': subject,
        'iss': issuer,
        'exp': int(exp_time),
        'scope': scope
    }
    token = jwt.encode(payload, secret_key, algorithm='HS256')

    return jsonify({
        'msg': 'token is generated',
        'access_token': str(token)}), 201


app.run(host='0.0.0.0', port=5001)