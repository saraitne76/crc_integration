from tensorflow.contrib import rnn, seq2seq
from utils import *
from tf_utils import *
from konlpy.tag import Mecab
import re
import os
import json


class Arguments:
    """
    Network hyper-parameter arguments set
    """

    def __init__(self,
                 cell_type: str,
                 ch_word2id: Dict,
                 kr_word2id: Dict,
                 enc_hiddens: List[int],
                 output_drop: bool,
                 state_drop: bool,
                 ch_emb_dim: int,
                 ch_emb_matrix: Optional[np.ndarray],
                 kr_emb_dim: int,
                 kr_emb_matrix: Optional[np.ndarray],
                 projection_layer: int):
        """
        :param cell_type: RNN cell type
        :param ch_word2id: Chinese Word-to-Index Dictionary
        :param kr_word2id: Korean Word-to-Index Dictionary
        :param enc_hiddens: RNN Hidden states, Length of list means the number of layers
        :param output_drop: Whether for RNN cell to use output drop out or not In train phase
        :param state_drop: Whether for RNN cell to use state drop out or not In train phase
        :param ch_emb_dim: Chinese word embedding dimension
        :param ch_emb_matrix: Chinese word embedding initialization, if None, using xavier initializer
        :param kr_emb_dim: Korean word embedding dimension
        :param kr_emb_matrix: Korean word embedding initialization, if None, using xavier initializer
        :param projection_layer: Decoder Projection Layer: Dense(0), transpose of target embedding matrix(1)
        """
        self.cell_type = cell_type
        self.ch_word2id = ch_word2id
        self.ch_vocab_size = len(self.ch_word2id)
        self.kr_word2id = kr_word2id
        self.kr_vocab_size = len(self.kr_word2id)
        self.enc_hiddens = enc_hiddens
        self.output_drop = output_drop
        self.state_drop = state_drop
        self.ch_emb_dim = ch_emb_dim
        self.ch_emb_matrix = ch_emb_matrix
        self.kr_emb_dim = kr_emb_dim
        self.kr_emb_matrix = kr_emb_matrix
        self.projection_layer = projection_layer
        return

    def print(self):
        if self.ch_emb_matrix is not None:
            ch_emb_mat_init = 'word2vec'
        else:
            ch_emb_mat_init = 'random'

        if self.kr_emb_matrix is not None:
            kr_emb_mat_init = 'word2vec'
        else:
            kr_emb_mat_init = 'random'

        if self.projection_layer == 0:
            projection_layer = 'Dens Layer'
        elif self.projection_layer == 1:
            projection_layer = 'Transpose of Target Embedding Matrix'
        else:
            raise ValueError('''Decoder Projection Layer: Dense(0), transpose of target embedding matrix(1)
                                But input: %s''' % self.projection_layer)

        s = """
            ===============Model Hyper Parameter===============
            RNN Cell Type: %s

            Chinese Vocabulary Size: %s
                first 5 examples: %s 
                end 5 examples: %s
                random 5 examples: %s

            Korean Vocabulary Size: %s
                first 5 examples: %s 
                end 5 examples: %s
                random 5 examples: %s

            Encoder Hidden: %s
            Decoder Hidden: %s
            RNN Output Drop-out: %s
            RNN State Drop-out: %s
            Chinese Embedding Size: %s
            Chinese Embedding Init: %s
            Korean Embedding Size: %s
            Korean Embedding Init: %s
            Decoder Output Projection: %s
            ===================================================
            \n\r""" % (self.cell_type,
                       self.ch_vocab_size,
                       list(self.ch_word2id.items())[:5],
                       list(self.ch_word2id.items())[-5:],
                       random.sample(list(self.ch_word2id.items()), 5),
                       self.kr_vocab_size,
                       list(self.kr_word2id.items())[:5],
                       list(self.kr_word2id.items())[-5:],
                       random.sample(list(self.kr_word2id.items()), 5),
                       self.enc_hiddens,
                       self.enc_hiddens[-1] * 2,
                       self.output_drop,
                       self.state_drop,
                       self.ch_emb_dim,
                       ch_emb_mat_init,
                       self.kr_emb_dim,
                       kr_emb_mat_init,
                       projection_layer)
        return s


class BaseTranslator:
    """
    Base Sequence-2-Sequence Neural Machine Translation Model
    """

    def __init__(self,
                 model_dir: str,
                 ckpt: str,
                 mecab_dic_path: Optional[str],
                 lexicon: Optional[List[str]]):
        args = pickle_load(os.path.join(model_dir, CONFIG_PICKLE))

        self.hyper_params = args
        self.ch_word2id = args.ch_word2id
        self.kr_word2id = args.kr_word2id
        self.ids2kr = dict_swap(self.kr_word2id)
        self.lexicon = lexicon

        self.mecab = Mecab(mecab_dic_path)

        self.global_step = tf.get_variable('GlobalStep', [],
                                           dtype=tf.int32,
                                           initializer=tf.initializers.constant(0),
                                           trainable=False)

        self.lr = tf.get_variable('LearningRate', [],
                                  dtype=tf.float32,
                                  initializer=tf.initializers.zeros(),
                                  trainable=False)
        self.lr_placeholder = tf.placeholder(tf.float32)
        self.lr_update = tf.assign(self.lr, self.lr_placeholder)

        self.kp = tf.get_variable('KeepProb', [],
                                  dtype=tf.float32,
                                  initializer=tf.initializers.ones(),
                                  trainable=False)
        self.kp_placeholder = tf.placeholder(tf.float32)
        self.kp_update = tf.assign(self.kp, self.kp_placeholder)

        self.weights_file = os.path.join(model_dir, ckpt, 'params')
        self._build_model(batch_size=1,
                          enc_max_len=None,
                          dec_max_len=1000,
                          beam_width=10)

        self.sess = tf.Session(config=get_tf_config())
        self.saver.restore(self.sess, os.path.join(model_dir, ckpt, 'params'))
        return

    def _build_model(self,
                     batch_size: Optional[int],
                     enc_max_len: Optional[int],
                     dec_max_len: Optional[int],
                     beam_width: Optional[int]):
        self.batch_size = batch_size
        self.enc_max_len = enc_max_len
        self.dec_max_len = dec_max_len
        self.beam_width = beam_width

        self._placeholders()

        self.prediction = self._seq2seq(enc_inp=self.enc_inp,
                                        enc_sub_inp=None,
                                        enc_seq_len=self.enc_seq_len,
                                        batch_size=self.batch_size)

        self.saver = tf.train.Saver()
        self.get_trainable_var = tf.trainable_variables()
        self.global_var_init = tf.global_variables_initializer()
        self.local_var_init = tf.local_variables_initializer()
        return

    def _placeholders(self):
        self.enc_inp = tf.placeholder(tf.int32, [self.batch_size, self.enc_max_len], name='EncoderInput')
        self.enc_seq_len = tf.placeholder(tf.int32, [self.batch_size], name='EncSeqLen')
        return

    def _seq2seq(self,
                 enc_inp: tf.Tensor,
                 enc_sub_inp: Optional[tf.Tensor],
                 enc_seq_len: tf.Tensor,
                 batch_size: int):

        output_drop = False
        state_drop = False

        enc_emb_inp, self.ch_emb_matrix = self.ch_word_emb(enc_inp, name='ChineseEmbedding')
        self.kr_emb_matrix = self.kr_word_emb(name='KoreanEmbedding')

        encoder_outputs, encoder_states = self.encoder(enc_emb_inp=enc_emb_inp,
                                                       seq_len=enc_seq_len,
                                                       keep_prob=self.kp,
                                                       output_drop=output_drop,
                                                       state_drop=state_drop,
                                                       name='Encoder')
        prediction = self.decoder(encoder_outputs=encoder_outputs,
                                  encoder_states=encoder_states,
                                  enc_seq_len=enc_seq_len,
                                  target_emb_matrix=self.kr_emb_matrix,
                                  keep_prob=self.kp,
                                  output_drop=output_drop,
                                  state_drop=state_drop,
                                  batch_size=batch_size,
                                  name='Decoder')
        return prediction

    def ch_word_emb(self,
                    idx_inp: tf.Tensor,
                    name: str):
        with tf.variable_scope(name):
            if self.hyper_params.ch_emb_matrix is not None:
                embed_init = tf.constant_initializer(self.hyper_params.ch_emb_matrix, verify_shape=True)
            else:
                embed_init = xavier_initializer()
            emb_matrix = tf.get_variable('EmbeddingMatrix',
                                         shape=[self.hyper_params.ch_vocab_size, self.hyper_params.ch_emb_dim],
                                         dtype=tf.float32,
                                         initializer=embed_init,
                                         trainable=True)
            embedding = tf.nn.embedding_lookup(emb_matrix, idx_inp)
        return embedding, emb_matrix

    def kr_word_emb(self,
                    name: str):
        with tf.variable_scope(name):
            emb_matrix = tf.get_variable('EmbeddingMatrix',
                                         shape=[self.hyper_params.kr_vocab_size, self.hyper_params.kr_emb_dim],
                                         dtype=tf.float32,
                                         initializer=xavier_initializer(),
                                         trainable=True)
        return emb_matrix

    def encoder(self,
                enc_emb_inp: tf.Tensor,
                seq_len: tf.Tensor,
                keep_prob,
                output_drop,
                state_drop,
                name='Encoder'):
        with tf.variable_scope(name):
            fw_cells = rnn_cells(cell_type=self.hyper_params.cell_type,
                                 hidden_list=self.hyper_params.enc_hiddens,
                                 keep_prob=keep_prob,
                                 output_drop=output_drop,
                                 state_drop=state_drop)
            bw_cells = rnn_cells(cell_type=self.hyper_params.cell_type,
                                 hidden_list=self.hyper_params.enc_hiddens,
                                 keep_prob=keep_prob,
                                 output_drop=output_drop,
                                 state_drop=state_drop)

            encoder_outputs, state_fw, state_bw = \
                rnn.stack_bidirectional_dynamic_rnn(cells_fw=fw_cells,
                                                    cells_bw=bw_cells,
                                                    inputs=enc_emb_inp,
                                                    sequence_length=seq_len,
                                                    dtype=tf.float32)

            state_c = tf.concat((state_fw[-1].c, state_bw[-1].c), 1)
            state_h = tf.concat((state_fw[-1].h, state_bw[-1].h), 1)
            encoder_states = rnn.LSTMStateTuple(c=state_c, h=state_h)
        return encoder_outputs, encoder_states

    def decoder(self,
                encoder_outputs: tf.Tensor,
                encoder_states: rnn.LSTMStateTuple,
                enc_seq_len: tf.Tensor,
                target_emb_matrix: tf.Variable,
                keep_prob,
                output_drop,
                state_drop,
                batch_size,
                name='Decoder'):
        # The Number Decoder Rnn Cell units
        num_units = self.hyper_params.enc_hiddens[-1] * 2
        with tf.variable_scope(name) as scope:
            decoder_cell = rnn_cell(self.hyper_params.cell_type,
                                    num_units,
                                    keep_prob,
                                    output_drop,
                                    state_drop)

            if self.hyper_params.projection_layer == 0:
                dec_oup_proj = tf.layers.Dense(self.hyper_params.kr_vocab_size, use_bias=False, name='DecOupProj')
            elif self.hyper_params.projection_layer == 1:
                dec_oup_proj = OutputLayer(num_inputs=num_units, embedding_matrix=target_emb_matrix)
            else:
                raise ValueError('''Decoder Projection Layer: Dense(0), transpose of target embedding matrix(1)
                                    But input: %s''' % self.hyper_params.projection_layer)

            tiled_encoder_output = seq2seq.tile_batch(encoder_outputs, multiplier=self.beam_width)
            tiled_encoder_state = seq2seq.tile_batch(encoder_states, multiplier=self.beam_width)
            tiled_seq_len = seq2seq.tile_batch(enc_seq_len, multiplier=self.beam_width)

            attention_mechanism = seq2seq.BahdanauAttention(num_units=self.hyper_params.enc_hiddens[-1] * 2,
                                                            memory=tiled_encoder_output,
                                                            memory_sequence_length=tiled_seq_len,
                                                            normalize=True)
            decoder_cell = seq2seq.AttentionWrapper(decoder_cell, attention_mechanism)
            initial_state = decoder_cell.zero_state(dtype=tf.float32, batch_size=batch_size * self.beam_width)
            initial_state = initial_state.clone(cell_state=tiled_encoder_state)
            decoder = seq2seq.BeamSearchDecoder(
                cell=decoder_cell,
                embedding=target_emb_matrix,
                start_tokens=tf.fill([batch_size], tf.constant(self.kr_word2id[GO])),
                end_token=tf.constant(self.kr_word2id[EOS]),
                initial_state=initial_state,
                beam_width=self.beam_width,
                output_layer=dec_oup_proj
            )
            outputs, _, _ = seq2seq.dynamic_decode(decoder,
                                                   maximum_iterations=self.dec_max_len,
                                                   output_time_major=False,
                                                   scope=scope)
            prediction = outputs.predicted_ids
            # print(prediction)
            return prediction

    def run(self, sentence: str):
        return

    def __del__(self):
        self.sess.close()
        return


not_ch = re.compile(r'[^\u2E80-\u2EFF\u3400-\u4DBF\u4E00-\u9FBF\uF900-\uFAFF]')
space = re.compile(r'\s{2,}')


def get_entity(line: str,
               ne_list: List[str],
               major_ne_list: Optional[List] = None) -> List[Tuple[str, int]]:
    """
    Find entity in Chinese sentence based on lexicon
    """
    entities = []
    temp = line

    if major_ne_list is not None:
        for ne in major_ne_list:
            flag = False
            s = 0
            while True:
                ne_idx = temp.find(ne, s)
                if ne_idx < 0:
                    break
                else:
                    flag = True
                    s = ne_idx + len(ne)
                    entities.append((ne, ne_idx))
            if flag:
                temp = temp.replace(ne, '-' * (len(ne)))

    for ne in ne_list:
        flag = False
        s = 0
        while True:
            ne_idx = temp.find(ne, s)
            if ne_idx < 0:
                break
            else:
                flag = True
                s = ne_idx + len(ne)
                entities.append((ne, ne_idx))
        if flag:
            temp = temp.replace(ne, '-'*(len(ne)))
    return entities


def tokenize_ch(ch: str, entities: List[Tuple[str, int]]):
    if len(entities) <= 0:
        return list(ch)
    else:
        entities.sort(key=lambda item: item[1])
        temp = []
        idx = 0
        entity_cnt = 0
        while True:
            if idx == entities[entity_cnt][1]:
                temp.append(entities[entity_cnt][0])
                idx += len(entities[entity_cnt][0])
                entity_cnt += 1
                if entity_cnt >= len(entities):
                    entity_cnt = len(entities) - 1
            elif ch[idx] == ' ':
                idx += 1
            else:
                temp.append(ch[idx])
                idx += 1
            if idx >= len(ch):
                break
        return temp


def clean_up_ch(ch: str, remove_mark=True):
    """
    Remove all character which is not Chinese.
    if remove_mark = True, all punctuation marks are replaced into ''
    if remove_mark = False, all punctuation marks are preserved as single space ' '
    """
    if remove_mark:
        return not_ch.sub('', ch)
    else:
        return space.sub(' ', not_ch.sub(' ', ch))
