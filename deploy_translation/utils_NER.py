"""
utility module.
"""
import numpy as np
import random
import pickle
import os
import os.path as pth
from typing import Union, Optional, List, Tuple, Dict
import openpyxl
import tensorflow as tf
from copy import deepcopy
from multiprocessing import Queue


BUCKET = np.arange(5.1, 51, 5)
TF2IDX = {'T': 0, 'F': 1}
BME2IDX = {'N': 0, 'B': 1, 'M': 2, 'E': 3, 'S': 4}


class LineData:
    """
    sentence by sentence data structure used model training.
    """
    def __init__(self,
                 ch_line: str,
                 kr_line: str,
                 x: np.ndarray,
                 y_tf: np.ndarray,
                 y_bme: np.ndarray,
                 loss_wt: np.ndarray,
                 seq_len: int,
                 whole_len: int,
                 entities: List[Tuple[str, int]]):
        """
        :param ch_line: original chinese sentence
        :param kr_line: original Korean sentence
        :param x: chinese -> respective index mapping sequence, input of model
        :param y_tf: True/False target label for each character
        :param y_bme: Begin, Middle, End, Single, None target label for each character
        :param loss_wt: weight value for each character to compute loss
        :param seq_len: valid sequence length (except padding)
        :param whole_len: overall sequence length (include padding)
        :param entities: named entities included in chinese sentence
                         maybe include location index of entity within a chinese sentence
        """
        self.ch_line = ch_line
        self.kr_line = kr_line
        self.x = x
        self.y_tf = y_tf
        self.y_bme = y_bme
        self.loss_wt = loss_wt
        self.seq_len = seq_len
        self.whole_len = whole_len
        self.entities = entities


class BatchData:
    """
    batch data of LineData
    """
    def __init__(self, samples: List[LineData]):
        """
        :param samples: List of LineData
        """
        self.size = len(samples)
        whole_len_b = np.unique([sample.whole_len for sample in samples])
        if len(whole_len_b) != 1:
            raise ValueError
        else:
            self.length = int(whole_len_b[0])
        self.ch_line_b = []
        self.kr_line_b = []
        self.x_b = np.zeros((self.size, self.length), np.int16)
        self.y_tf_b = np.zeros((self.size, self.length), np.int16)
        self.y_bme_b = np.zeros((self.size, self.length), np.int16)
        self.loss_wt_b = np.zeros((self.size, self.length), np.int16)
        self.seq_len_b = np.zeros(self.size, np.int16)
        self.entities_b = []
        for i, sample in enumerate(samples):
            self.ch_line_b.append(sample.ch_line)
            self.kr_line_b.append(sample.kr_line)
            self.x_b[i, :] = sample.x
            self.y_tf_b[i, :] = sample.y_tf
            self.y_bme_b[i, :] = sample.y_bme
            self.loss_wt_b[i, :] = sample.loss_wt
            self.seq_len_b[i] = sample.seq_len
            self.entities_b.append(sample.entities)


def print_write(s, file, mode=None):
    if isinstance(file, str):
        if mode is None:
            mode = 'a'
        f = open(file, mode)
        print(s, end='')
        f.write(s)
        f.close()
    else:
        print(s, end='')
        file.write(s)


def dict_swap(my_dic: Dict):
    return {v: k for k, v in my_dic.items()}


def harmonic_mean(a, b):
    return (2*a*b)/(a+b)


def get_tf_config():
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    return config


def lstm_cell(hidden, keep_prob):
    cell = tf.nn.rnn_cell.BasicLSTMCell(hidden, state_is_tuple=True)
    cell = tf.nn.rnn_cell.DropoutWrapper(cell, output_keep_prob=keep_prob)
    return cell


def text_readlines(file_path):
    f = open(file_path, 'r', encoding='utf-8')
    t = f.readlines()
    f.close()
    return t


def text_read(file_path):
    f = open(file_path, 'r', encoding='utf-8')
    t = f.read()
    f.close()
    return t


def chinese_regularizer(line, dic):
    """
    Change all chinese character unified
    :param line: Chinese or Korean that has chinese character sentence
    :param dic: compatibility -> unified chinese character dictionary
    :return: unified chinese sentence
    """
    line_out = ''
    for c in line:
        try:
            line_out = line_out + dic[c]
        except KeyError:
            line_out = line_out + c
    return line_out


def read_dic(path):
    f = open(path, 'r', encoding='utf-8')
    lines = f.readlines()[2:]
    f.close()
    dic = {}
    for l in lines:
        s = l.split()
        dic[s[0]] = s[1]
    return dic


def pronounce(ne, p, pi):
    h = pi[ne[0]]
    for i, c in enumerate(ne[1:]):
        h = h + p[c]
    return h


def read_xlsx(xlsx_path, column):
    wb = openpyxl.load_workbook(xlsx_path)
    ws = wb.active
    data = []
    if len(column) == 1:
        for r in ws.rows:
            data.append(r[column[0]].value)
    else:
        for r in ws.rows:
            rr = []
            for c in column:
                rr.append(r[c].value)
            data.append(rr)
    wb.close()
    return data


def read_xlsx_ne(xlsx_path):
    wb = openpyxl.load_workbook(xlsx_path)
    ws = wb.active
    data = []
    for row in ws.rows:
        for cell in row[2:]:
            if cell.value is None:
                break
            # print(cell.value)
            data.append(cell.value)
    wb.close()
    return data


def read_total_xlsx(xlsx_path):
    wb = openpyxl.load_workbook(xlsx_path)
    ws = wb.active
    data = []
    for row in ws.rows:
        row_data = []
        for cell in row:
            if cell.value is None:
                break
            # print(cell.value)
            row_data.append(cell.value)
        data.append(row_data)
    wb.close()
    return data


def entity_form(target: List[List[Tuple[str, int]]],
                prediction: List[List[Tuple[str, int]]]) -> Tuple[float, float, float]:
    assert len(target) == len(prediction)
    n_sentence = len(target)
    true_ne = deepcopy(target)
    pred_ne = deepcopy(prediction)
    correct_ne = []

    for i in range(n_sentence):
        correct_ne.append([])
        for ne in true_ne[i]:
            if pred_ne[i].count(ne) > 0:
                correct_ne[i].append(pred_ne[i].pop(pred_ne[i].index(ne)))

    for i in range(n_sentence):
        for ne in correct_ne[i]:
            true_ne[i].remove(ne)

    true_positive = 0
    false_positive = 0
    false_negative = 0
    for i in range(n_sentence):
        true_positive += len(correct_ne[i])
        false_positive += len(pred_ne[i])
        false_negative += len(true_ne[i])

    return score(true_positive, false_positive, false_negative)


def surface_form(target: List[List[Tuple[str, int]]],
                 prediction: List[List[Tuple[str, int]]]) -> Tuple[float, float, float]:
    assert len(target) == len(prediction)
    n_sentence = len(target)
    true_ne = []
    pred_ne = []

    for i in range(n_sentence):
        for ne in target[i]:
            true_ne.append(ne[0])
        for ne in prediction[i]:
            pred_ne.append(ne[0])

    true_ne = list(set(true_ne))
    pred_ne = list(set(pred_ne))
    correct_ne = []
    for ne in true_ne:
        if pred_ne.count(ne) > 0:
            correct_ne.append(pred_ne.pop(pred_ne.index(ne)))

    for ne in correct_ne:
        true_ne.remove(ne)

    true_positive = len(correct_ne)
    false_positive = len(pred_ne)
    false_negative = len(true_ne)

    return score(true_positive, false_positive, false_negative)


def score(tp: int, fp: int, fn: int) -> Tuple[float, float, float]:
    try:
        precision = tp / (tp + fp)
    except ZeroDivisionError:
        precision = 0

    try:
        recall = tp / (tp + fn)
    except ZeroDivisionError:
        recall = 0

    try:
        f1 = harmonic_mean(precision, recall)
    except ZeroDivisionError:
        f1 = 0

    return f1, precision, recall


def uni2dec(c):
    encoded = list(c.encode('unicode-escape'))[2:]
    if not encoded:
        return 0
    h = ''
    for c in encoded:
        h += chr(c)
    return int(h, 16)


def unicode_classify(c):
    dec = uni2dec(c)
    if (int('4E00', 16) <= dec <= int('9FFF', 16) or
            int('3400', 16) <= dec <= int('4DBF', 16) or
            int('F900', 16) <= dec <= int('FAFF', 16)):
        return 0    # Chinese
    elif int('AC00', 16) <= dec <= int('D7AF', 16):
        return 1    # Korean
    else:
        return 2    # else


def pickle_store(obj, path):
    f = open(path, 'wb')
    pickle.dump(obj, f)
    f.close()
    return


def pickle_load(path):
    f = open(path, 'rb')
    a = pickle.load(f)
    f.close()
    return a


def read_data(folder):
    files = os.listdir(folder)
    d = []
    for f in files:
        if f.split('.')[-1] == 'p':
            d = d + pickle_load(pth.join(folder, f))
    return d


def pad_line(line, bucket):
    length = len(line)
    temp = np.append(bucket, [length])
    temp.sort()
    kk = np.where(temp == float(length))[0][0]
    if kk < bucket.shape[0]:
        padded = line + 'P' * (int(bucket[kk]) - length)
    else:
        padded = line[:int(bucket[-1])]
    return padded


def line_split(line: str, max_len: int, del_redundant=True):
    splitted = []
    p = list(range(0, len(line), max_len//2))
    for i in range(len(p)):
        try:
            splitted.append(line[p[i]:p[i+2]])
        except IndexError:
            splitted.append(line[p[i]:])
            break
    if del_redundant:
        return remove_redundant_line(splitted)
    else:
        return splitted


def remove_single_entity(ne_list):
    new = []
    for n in ne_list:
        if isinstance(n, tuple):
            if len(n[0]) > 1:
                new.append(n)
        else:
            if len(n) > 1:
                new.append(n)
    return new


def line2data(line: str,
              ne_list: List[str],
              ch2idx: Dict,
              bme2idx: Dict,
              tf2idx: Dict) -> Tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray, int, List[Tuple[str, int]]]:
    """
    chinese sentence -> RNN model input data
    :param line: chinese sentence
    :param ne_list: named entity list that search in chinese sentence.
                    it must be sorted by named entity length
    :param ch2idx: chinese character -> number mapping dictionary
    :param bme2idx: BME label -> number mapping dictionary
    :param tf2idx: True/False -> number mapping dictionary
    :param with_ne_idx: return Named Entity list with line index
    :return: RNN model input(sequence of chinese index number) of buckets
             RNN model TF target
             RNN model BME target
             RNN model loss weight
             RNN model valid sequence length (except padding)
             named entity list for this chinese sentence
    """
    # line = line.rstrip()
    length = len(line)
    x = np.zeros(length, dtype=np.int16)
    for i, c in enumerate(line):
        try:
            x[i] = ch2idx[c]
        except KeyError:
            x[i] = ch2idx['U']
    y_bme = np.zeros(length, dtype=np.int16)
    y_bme[:] = bme2idx['N']
    y_tf = np.zeros(length, dtype=np.int16)
    y_tf[:] = tf2idx['F']
    entities = []
    temp = line
    for ne in ne_list:
        flag = False
        s = 0
        while True:
            ne_idx = temp.find(ne, s)
            if ne_idx < 0:
                break
            else:
                flag = True
                s = ne_idx + len(ne)
                if len(ne) == 1:
                    y_bme[ne_idx] = bme2idx['S']
                else:
                    y_bme[ne_idx] = bme2idx['B']
                    y_bme[ne_idx+len(ne)-1] = bme2idx['E']
                    y_bme[ne_idx+1:ne_idx+len(ne)-1] = bme2idx['M']
                y_tf[ne_idx:ne_idx + len(ne)] = tf2idx['T']
                entities.append((ne, ne_idx))
        if flag:
            temp = temp.replace(ne, '-'*(len(ne)))
    seq_len = line.find('P')
    if seq_len < 0:
        seq_len = length
    wt = np.zeros(length, dtype=np.float16)
    wt[:seq_len] = 1.
    return x, y_tf, y_bme, wt, seq_len, entities


def bucketting(data: List, bucket: Union[List, np.ndarray]) -> Tuple[List[List[LineData]], np.ndarray]:
    """
    Bucket list of LineData as specified bucket
    :param data: List of Line Data
    :param bucket: max length list of sequences
    :return: bucketted data & the probability each bucket selected
    """
    bucketted = []
    for i in range(bucket.shape[0]):
        bucketted.append([])

    for d in data:
        seq_len = d.seq_len
        temp = np.append(bucket, [seq_len])
        temp.sort()
        kk = np.where(temp == float(seq_len))[0][0]
        if kk < bucket.shape[0]:
            bucketted[kk].append(d)

    ndata = len(data)
    nbucket = len(bucketted)
    bucket_prob = np.zeros(nbucket)
    for i in range(nbucket):
        bucket_prob[i] = len(bucketted[i]) / ndata
    return bucketted, bucket_prob


def get_batch(bucketted: List[List[LineData]], bucket_prob: np.ndarray, batch_size: int) -> BatchData:
    """
    Get batch data using bucketted LineData
    :param bucketted: bucketted LineData (number of bucket, number of data in the bucket)
    :param bucket_prob: The probability that the bucket will be selected.
                        according to the number of data in the bucket.
                        the number of data in the bucket is increase, the probability the bucket selected is increase.
    :param batch_size: the number of data in the batch
    :return: BatchData
    """
    t = np.random.choice(len(bucketted), 1, False, bucket_prob)[0]
    try:
        samples = random.sample(bucketted[t], batch_size)
    except ValueError:
        samples = random.sample(bucketted[t], len(bucketted[t]))
    return BatchData(samples)


def pred2ne(line: str, prediction: Union[np.ndarray, List, Tuple], bme2idx: Dict) -> List[Tuple[str, int]]:
    """
    using model prediction and input chinese sentence,
    extract Named Entity.
    :param line: chinese sentence.
    :param prediction: model prediction
    :param bme2idx: BME label -> number mapping dictionary
    :return: extracted Named Entity
    """
    seq_len = line.find('P')
    if seq_len < 0:
        seq_len = len(line)
    line = line[:seq_len]
    n = ''
    ne = []
    ne_idx = -1
    flag = False
    for i, c in enumerate(line):
        if prediction[i] == bme2idx['N']:
            n = ''
            flag = False
        elif prediction[i] == bme2idx['B']:
            ne_idx = i
            n = c
            flag = True
        elif prediction[i] == bme2idx['M'] and flag:
            n = n + c
        elif prediction[i] == bme2idx['E'] and flag:
            n = n + c
            ne.append((n, ne_idx))
            flag = False
            n = ''
        elif prediction[i] == bme2idx['S']:
            ne.append((c, i))
            n = ''
            flag = False
        else:
            n = ''
    return ne


def remove_redundant_line(lines, check=0):
    new_lines = []
    for line in lines:
        f = False
        for c in line:
            if unicode_classify(c) == check:    # 0 hanja, 1 hangeul
                f = True
                break
        if f:
            new_lines.append(line)
    return new_lines


def extract_entities(kr_line, with_ne_idx=True):
    entity = ''
    entity_list = []
    flag = False
    entity_idx = -1
    for i, c in enumerate(kr_line):
        char_catgry = unicode_classify(c)
        if char_catgry == 0 and not flag:
            entity_idx = i
            entity = c
            flag = True
        elif char_catgry == 0 and flag:
            entity += c
        elif char_catgry != 0 and flag:
            if with_ne_idx:
                entity_list.append((entity, entity_idx))
            else:
                entity_list.append(entity)
            flag = False
        else:
            pass
    return entity_list


def data_write(target: str,
               ch2idx: Dict,
               gazetteer: List[str],
               pickle_name: str,
               ch_kr_list: List,
               q: Queue,
               linebyline=False):
    data_list = []
    while q.qsize() > 0 or not q.empty():
        ch_kr = ch_kr_list[q.get()]
        if linebyline:
            x, y_tf, y_bme, wt, seq_len, entities = line2data(ch_kr[0],
                                                              gazetteer,
                                                              ch2idx,
                                                              BME2IDX,
                                                              TF2IDX)
            whole_len = np.unique([len(ch_kr[0]), len(x), len(y_tf), len(y_bme), len(wt)])
            if len(whole_len) != 1:
                raise ValueError
            data = LineData(ch_kr[0], ch_kr[1], x, y_tf, y_bme, wt, seq_len, int(whole_len[0]), entities)
            data_list.append(data)

        else:
            splitted = line_split(ch_kr[0], int(BUCKET[-1]), del_redundant=True)
            for line in splitted:
                padded = pad_line(line, BUCKET)
                x, y_tf, y_bme, wt, seq_len, entities = line2data(padded,
                                                                  gazetteer,
                                                                  ch2idx,
                                                                  BME2IDX,
                                                                  TF2IDX)
                whole_len = np.unique([len(padded), len(x), len(y_tf), len(y_bme), len(wt)])
                if len(whole_len) != 1:
                    raise ValueError
                data = LineData(padded, ch_kr[1], x, y_tf, y_bme, wt, seq_len, int(whole_len[0]), entities)
                data_list.append(data)

        print('\rRemain Data %d' % (q.qsize()), end='')
        if q.qsize() < 1 or q.empty():
            break
    if linebyline:
        os.makedirs(os.path.join(target, 'linebyline'), exist_ok=True)
        pickle_store(data_list, os.path.join(target, 'linebyline', pickle_name))
    else:
        os.makedirs(os.path.join(target, 'split_pad'), exist_ok=True)
        pickle_store(data_list, os.path.join(target, 'split_pad', pickle_name))
    return
