from base_model import *

LOG_DIR = 'Logs'
PARAMS_DIR = 'Params'


class Translator(BaseTranslator):
    """
    Model 2 Description
    Chinese Character Embedding
    Korean token Embedding
    Sequence-to-Sequence with BahdanauAttention Model
    """

    def __init__(self,
                 model_dir: str,
                 ckpt: str,
                 mecab_dic_path: Optional[str]):
        super().__init__(model_dir=model_dir, ckpt=ckpt, mecab_dic_path=mecab_dic_path, lexicon=None)
        return

    def run(self, sentence: str):
        """
        :param sentence: Chinese sentence with on Chinese character(no any marks)
        :return: Korean tokens (translated)
        """
        sent = clean_up_ch(sentence, remove_mark=True)
        char_seq = list(sent)
        seq_len = len(char_seq)
        if seq_len < 1:
            result = {'korean': 'invalid input : %s\n' % sentence}
        else:
            ch_ids = seq2ids(char_seq, self.ch_word2id)

            feed = {self.enc_inp: [ch_ids],
                    self.enc_seq_len: [seq_len]}

            prediction = self.sess.run(self.prediction, feed_dict=feed)
            # [batch, seq_length, beam_width]
            prediction = prediction[0, :, 0]
            kr_seq = ids2seq(prediction, self.ids2kr, EOS)
            result = {'korean': detokenize(kr_seq, self.mecab)}
        return json.dumps(result)

    def runs(self, sentences: List[str]):
        """
        :param sentences: list of Chinese sentence
        :return: Korean tokens (translated)
        """
        kr = ''
        num_sent = len(sentences)
        for i, sentence in enumerate(sentences):
            sent = clean_up_ch(sentence, remove_mark=True)
            char_seq = list(sent)
            seq_len = len(char_seq)
            print('%d/%d, seq_len: %d, %s' % (i, num_sent, seq_len, sent))
            if seq_len < 1:
                pass
            else:
                ch_ids = seq2ids(char_seq, self.ch_word2id)

                feed = {self.enc_inp: [ch_ids],
                        self.enc_seq_len: [seq_len]}

                prediction = self.sess.run(self.prediction, feed_dict=feed)
                # [batch, seq_length, beam_width]
                prediction = prediction[0, :, 0]
                kr_seq = ids2seq(prediction, self.ids2kr, EOS)
                kr = kr + ' ' + detokenize(kr_seq, self.mecab)
        result = {'korean': kr}
        return json.dumps(result)


if __name__ == '__main__':
    model = Translator(model_dir='M2_S1', ckpt='step-100000')
    print(model.run('聞方伯巡過本邑'))
    del model
