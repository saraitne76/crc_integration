from base_model import *

LOG_DIR = 'Logs'
PARAMS_DIR = 'Params'


class Arguments:
    """
    Network hyper-parameter arguments set
    """

    def __init__(self,
                 cell_type: str,
                 ch_word2id: Dict,
                 ch_char2id: Dict,
                 char_max_len: int,
                 kr_word2id: Dict,
                 enc_hiddens: List[int],
                 output_drop: bool,
                 state_drop: bool,
                 ch_emb_dim: int,
                 ch_emb_matrix: Optional[np.ndarray],
                 ch_char_emb_dim: int,
                 ch_char_emb_matrix: Optional[np.ndarray],
                 kr_emb_dim: int,
                 kr_emb_matrix: Optional[np.ndarray],
                 projection_layer: int):
        """
        :param cell_type: RNN cell type
        :param ch_word2id: Chinese Word-to-Index Dictionary
        :param ch_char2id: Chinese Character-to-Index Dictionary
        :param char_max_len: Character max length in one Chinese word
        :param kr_word2id: Korean Word-to-Index Dictionary
        :param enc_hiddens: RNN Hidden states, Length of list means the number of layers
        :param output_drop: Whether for RNN cell to use output drop out or not In train phase
        :param state_drop: Whether for RNN cell to use state drop out or not In train phase
        :param ch_emb_dim: Chinese word embedding dimension
        :param ch_emb_matrix: Chinese word embedding initialization, if None, using xavier initializer
        :param ch_char_emb_dim: Chinese character embedding dimension
        :param ch_char_emb_matrix: Chinese word embedding initialization, if None, using xavier initializer
        :param kr_emb_dim: Korean word embedding dimension
        :param kr_emb_matrix: Korean word embedding initialization, if None, using xavier initializer
        :param projection_layer: Decoder Projection Layer: Dense(0), transpose of target embedding matrix(1)
        """
        self.cell_type = cell_type

        self.ch_word2id = ch_word2id
        self.ch_vocab_size = len(self.ch_word2id)

        self.ch_char2id = ch_char2id
        self.ch_char_vocab_size = len(self.ch_char2id)
        self.char_max_len = char_max_len

        self.kr_word2id = kr_word2id
        self.kr_vocab_size = len(self.kr_word2id)

        self.enc_hiddens = enc_hiddens
        self.output_drop = output_drop
        self.state_drop = state_drop

        self.ch_emb_dim = ch_emb_dim
        self.ch_emb_matrix = ch_emb_matrix

        self.ch_char_emb_dim = ch_char_emb_dim
        self.ch_char_emb_matrix = ch_char_emb_matrix

        self.kr_emb_dim = kr_emb_dim
        self.kr_emb_matrix = kr_emb_matrix
        self.projection_layer = projection_layer
        return

    def print(self):
        if self.ch_emb_matrix is not None:
            ch_emb_mat_init = 'word2vec'
        else:
            ch_emb_mat_init = 'random'

        if self.ch_char_emb_matrix is not None:
            ch_char_emb_mat_init = 'word2vec'
        else:
            ch_char_emb_mat_init = 'random'

        if self.kr_emb_matrix is not None:
            kr_emb_mat_init = 'word2vec'
        else:
            kr_emb_mat_init = 'random'

        if self.projection_layer == 0:
            projection_layer = 'Dens Layer'
        elif self.projection_layer == 1:
            projection_layer = 'Transpose of Target Embedding Matrix'
        else:
            raise ValueError('''Decoder Projection Layer: Dense(0), transpose of target embedding matrix(1)
                                But input: %s''' % self.projection_layer)

        s = """
            ===============Model Hyper Parameter===============
            RNN Cell Type: %s

            Chinese Word Vocabulary Size: %s
                first 5 examples: %s 
                end 5 examples: %s
                random 5 examples: %s

            Chinese Character Vocabulary Size: %s
                first 5 examples: %s 
                end 5 examples: %s
                random 5 examples: %s

            Korean Vocabulary Size: %s
                first 5 examples: %s 
                end 5 examples: %s
                random 5 examples: %s

            Encoder Hidden: %s
            Decoder Hidden: %s
            RNN Output Drop-out: %s
            RNN State Drop-out: %s
            Chinese Embedding Size: %s
            Chinese Embedding Init: %s
            Chinese Character Embedding Size: %s
            Chinese Character Embedding Init: %s
            Chinese Character Maximum Length: %s
            Korean Embedding Size: %s
            Korean Embedding Init: %s
            Decoder Output Projection: %s
            ===================================================
            \n\r""" % (self.cell_type,

                       self.ch_vocab_size,
                       list(self.ch_word2id.items())[:5],
                       list(self.ch_word2id.items())[-5:],
                       random.sample(list(self.ch_word2id.items()), 5),

                       self.ch_char_vocab_size,
                       list(self.ch_char2id.items())[:5],
                       list(self.ch_char2id.items())[-5:],
                       random.sample(list(self.ch_char2id.items()), 5),

                       self.kr_vocab_size,
                       list(self.kr_word2id.items())[:5],
                       list(self.kr_word2id.items())[-5:],
                       random.sample(list(self.kr_word2id.items()), 5),

                       self.enc_hiddens,
                       self.enc_hiddens[-1] * 2,
                       self.output_drop,
                       self.state_drop,

                       self.ch_emb_dim,
                       ch_emb_mat_init,

                       self.ch_char_emb_dim,
                       ch_char_emb_mat_init,
                       self.char_max_len,

                       self.kr_emb_dim,
                       kr_emb_mat_init,
                       projection_layer)
        return s


class Translator(BaseTranslator):
    """
    Model 3 Description
    Hybrid Chinese tokens and characters
                    tokens + summation of characters in tokens
    Korean token Embedding
    Sequence-to-Sequence with BahdanauAttention Model
    """
    def __init__(self,
                 model_dir: str,
                 ckpt: str,
                 mecab_dic_path: Optional[str],
                 lexicon_path: str):
        lexicon = list(read_dic(lexicon_path, start_line=0).keys())
        lexicon.sort(key=lambda x: len(x), reverse=True)

        args = pickle_load(os.path.join(model_dir, CONFIG_PICKLE))
        self.ch_char2id = args.ch_char2id
        self.char_max_len = args.char_max_len

        super().__init__(model_dir=model_dir, ckpt=ckpt, mecab_dic_path=mecab_dic_path, lexicon=lexicon)
        return

    def _build_model(self,
                     batch_size: Optional[int],
                     enc_max_len: Optional[int],
                     dec_max_len: Optional[int],
                     beam_width: Optional[int]):
        self.batch_size = batch_size
        self.enc_max_len = enc_max_len
        self.dec_max_len = dec_max_len
        self.beam_width = beam_width

        self._placeholders()

        self.prediction = self._seq2seq(enc_inp=self.enc_inp,
                                        enc_sub_inp=self.enc_sub_inp,
                                        enc_seq_len=self.enc_seq_len,
                                        batch_size=self.batch_size)

        self.saver = tf.train.Saver()
        self.get_trainable_var = tf.trainable_variables()
        self.global_var_init = tf.global_variables_initializer()
        self.local_var_init = tf.local_variables_initializer()
        return

    def _placeholders(self):
        self.enc_inp = tf.placeholder(tf.int32, [self.batch_size, self.enc_max_len], name='EncoderInput')
        self.enc_sub_inp = tf.placeholder(tf.int32,
                                          [self.batch_size, self.enc_max_len, self.char_max_len],
                                          name='EncoderSubInput')
        self.enc_seq_len = tf.placeholder(tf.int32, [self.batch_size], name='EncSeqLen')

    def _seq2seq(self,
                 enc_inp: tf.Tensor,
                 enc_sub_inp: Optional[tf.Tensor],
                 enc_seq_len: tf.Tensor,
                 batch_size: int):

        output_drop = False
        state_drop = False

        with tf.variable_scope('EncoderInput'):
            word_emb, self.ch_word_emb_matrix = self.ch_word_emb(enc_inp, name='ChineseWordEmb')
            char_emb, self.ch_char_emb_matrix = self.ch_char_emb(enc_sub_inp, name='ChineseCharEmb')
            enc_emb_inp = tf.concat([word_emb, char_emb], axis=2)

        self.kr_word_emb_matrix = self.kr_word_emb(name='KoreanWordEmb')

        encoder_outputs, encoder_states = self.encoder(enc_emb_inp=enc_emb_inp,
                                                       seq_len=enc_seq_len,
                                                       keep_prob=self.kp,
                                                       output_drop=output_drop,
                                                       state_drop=state_drop,
                                                       name='Encoder')
        prediction = self.decoder(encoder_outputs=encoder_outputs,
                                  encoder_states=encoder_states,
                                  enc_seq_len=enc_seq_len,
                                  target_emb_matrix=self.kr_word_emb_matrix,
                                  keep_prob=self.kp,
                                  output_drop=output_drop,
                                  state_drop=state_drop,
                                  batch_size=batch_size,
                                  name='Decoder')
        return prediction

    def ch_char_emb(self,
                    idx_inp: tf.Tensor,
                    name: str):
        with tf.variable_scope(name):
            emb_matrix = comp_embedding_matrix(name='EmbeddingMatrix',
                                               shape=[self.hyper_params.ch_char_vocab_size,
                                                      self.hyper_params.ch_char_emb_dim],
                                               init_mat=self.hyper_params.ch_char_emb_matrix)
            # [batch_size, length, component length, embedding size]
            char_emb = comp_embedding_lookup(emb_matrix, idx_inp)
            # [batch_size, length, embedding size]
            embedding = tf.reduce_sum(char_emb, axis=2)

        return embedding, emb_matrix

    def run(self, sentence: str):
        """
        :param sentence: Chinese sentence with on Chinese character(no any marks)
        :return: Korean tokens (translated)
        """
        sent = clean_up_ch(sentence, remove_mark=True)
        entities = get_entity(sent, self.lexicon)
        ch_tok = tokenize_ch(sent, entities)
        seq_len = len(ch_tok)
        if seq_len < 1:
            result = {'korean': 'invalid input : %s\n' % sentence}
        else:
            ch_ids = seq2ids(ch_tok, self.ch_word2id)
            ch_sub_ids = np.zeros([1, seq_len, self.char_max_len])

            for j, token in enumerate(ch_tok):
                chars = list(token)[:self.char_max_len]
                ch_sub_ids[0, j, :len(chars)] = seq2ids(chars, self.ch_char2id)

            feed = {self.enc_inp: [ch_ids],
                    self.enc_sub_inp: ch_sub_ids,
                    self.enc_seq_len: [seq_len]}

            prediction = self.sess.run(self.prediction, feed_dict=feed)
            # [batch, seq_length, beam_width]
            prediction = prediction[0, :, 0]
            kr_seq = ids2seq(prediction, self.ids2kr, EOS)
            result = {'korean': detokenize(kr_seq, self.mecab)}
        return json.dumps(result)


if __name__ == '__main__':
    model = Translator(model_dir='M3_S1', ckpt='step-45000', lexicon_path='lexicon.txt')
    print(model.run('己嶺東在子到日生避五日戊主都日行日事嫌于發日移己十自卒天驛所臺爲驛十月馬月凍東卒分午字留世舊到早宿晴遞出袂年望義爲邊馬甚不爲良驛淨知有爲鄭壹贊偕宿官城梁東主于晴佐十南後之金主驛朝追矣宿知籍點陽家自話是典洛驛洪到到初寒來還縣安二縣遞龍書偕前辭吳來遞暮餞馬己是年爲正爲有拜氏書以大寒矣接官十之來'))
    del model
