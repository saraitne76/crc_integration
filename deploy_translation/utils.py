"""
utility module.
"""
import openpyxl
from typing import Dict, List, Tuple, Union, Optional
import pickle
import numpy as np
import random
from gensim.models import Word2Vec


PAD = 'PAD'
UNK = 'UNK'
GO = 'GO'
EOS = 'EOS'
# SPECIAL_WORDS = [PAD, UNK, GO, EOS]

CONFIG_TXT = 'config.txt'
CONFIG_PICKLE = 'config.p'


class PairData:
    def __init__(self,
                 ch_sent: str,
                 ch_tok: List[str],
                 kr_sent: str,
                 kr_tok: List[str]):
        self.ch_sent = ch_sent
        self.ch_tok = ch_tok
        self.ch_seq_len = len(self.ch_tok)
        self.kr_sent = kr_sent
        self.kr_tok = kr_tok
        self.kr_seq_len = len(self.kr_tok)

    def print(self):
        s = """
            Chinese
                sentence: %s
                token: %s
                sequence length: %s
            Korean
                sentence: %s
                token: %s
                sequence length: %s
            \n\r""" % (self.ch_sent, self.ch_tok, self.ch_seq_len,
                       self.kr_sent, self.kr_tok, self.kr_seq_len)
        return s


class BaseDataSet:
    def __init__(self,
                 enc_max_len: int,
                 dec_max_len: int,
                 ch_word2id: Dict,
                 kr_word2id: Dict):
        self.enc_max_len = enc_max_len
        # include GO or EOS token
        self.dec_max_len = dec_max_len

        self.ch_word2id = ch_word2id
        self.kr_word2id = kr_word2id

        self._sequential_indices = 0

        self.num_data = None
        self.full_indices = None
        self.enc_inputs = None
        self.enc_seq_len = None

        self.dec_inputs = None
        self.dec_targets = None
        self.dec_seq_len = None

    def make(self,
             pairs: List[PairData]):

        new_pairs = list()
        for p in pairs:
            # GO or EOS token will be appended to Korean sequence
            if 1 <= p.ch_seq_len <= self.enc_max_len and 1 <= p.kr_seq_len <= self.dec_max_len-1:
                new_pairs.append(p)
        del pairs

        self.num_data = len(new_pairs)
        self.full_indices = list(range(self.num_data))
        self.enc_inputs = np.zeros([self.num_data, self.enc_max_len], dtype=np.uint16)
        self.enc_seq_len = np.zeros([self.num_data], dtype=np.uint16)

        self.dec_inputs = np.zeros([self.num_data, self.dec_max_len], dtype=np.uint16)
        self.dec_targets = np.zeros([self.num_data, self.dec_max_len], dtype=np.uint16)
        self.dec_seq_len = np.zeros([self.num_data], dtype=np.uint16)

        for i, p in enumerate(new_pairs):
            # ch_ids = list(map(lambda x: self.ch_word2id.get(x, self.ch_word2id[UNK]), p.ch_tok))
            ch_ids = seq2ids(p.ch_tok, self.ch_word2id)
            self.enc_inputs[i, :p.ch_seq_len] = ch_ids
            self.enc_seq_len[i] = p.ch_seq_len

            # kr_ids = list(map(lambda x: self.kr_word2id.get(x, self.kr_word2id[UNK]), p.kr_tok))
            kr_ids = seq2ids(p.kr_tok, self.kr_word2id)
            self.dec_inputs[i, :p.kr_seq_len+1] = [self.kr_word2id[GO]] + kr_ids
            self.dec_targets[i, :p.kr_seq_len+1] = kr_ids + [self.kr_word2id[EOS]]
            self.dec_seq_len[i] = p.kr_seq_len + 1

    def get_batch(self, batch_size) -> Tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray, np.ndarray]:
        indices = random.sample(self.full_indices, batch_size)
        out = (self.enc_inputs[indices, :],
               self.enc_seq_len[indices],
               self.dec_inputs[indices, :],
               self.dec_targets[indices, :],
               self.dec_seq_len[indices])
        return out

    @property
    def sequential_indices(self):
        return self._sequential_indices

    @sequential_indices.setter
    def sequential_indices(self, idx=0):
        self._sequential_indices = idx

    def get_sequential_batch(self, batch_size) -> \
            Tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray, np.ndarray]:
        s = self.sequential_indices
        e = self.sequential_indices + batch_size
        # If remain data is not enough, < batch size, drop remain data, finish sequential batch
        out = (self.enc_inputs[s:e, :],
               self.enc_seq_len[s:e],
               self.dec_inputs[s:e, :],
               self.dec_targets[s:e, :],
               self.dec_seq_len[s:e])
        self.sequential_indices += batch_size
        return out


class DataSet1(BaseDataSet):
    """
    Encoder Input: Sequence of Chinese Tokens
    Decoder Input: Sequence of Korean Tokens
    """
    def __init__(self,
                 enc_max_len: int,
                 dec_max_len: int,
                 ch_word2id: Dict,
                 kr_word2id: Dict):
        super().__init__(enc_max_len, dec_max_len, ch_word2id, kr_word2id)
        return


class DataSet2(BaseDataSet):
    """
    Encoder Input: Sequence of Chinese Characters
    Decoder Input: Sequence of Korean Tokens
    """
    def __init__(self,
                 enc_max_len: int,
                 dec_max_len: int,
                 ch_word2id: Dict,
                 kr_word2id: Dict):
        super().__init__(enc_max_len, dec_max_len, ch_word2id, kr_word2id)
        return

    def make(self,
             pairs: List[PairData]):

        new_pairs = list()
        for p in pairs:
            # GO or EOS token will be appended to Korean sequence
            if 1 <= len(p.ch_sent) <= self.enc_max_len and 1 <= p.kr_seq_len <= self.dec_max_len-1:
                new_pairs.append(p)
        del pairs

        self.num_data = len(new_pairs)
        self.full_indices = list(range(self.num_data))
        self.enc_inputs = np.zeros([self.num_data, self.enc_max_len], dtype=np.uint16)
        self.enc_seq_len = np.zeros([self.num_data], dtype=np.uint16)

        self.dec_inputs = np.zeros([self.num_data, self.dec_max_len], dtype=np.uint16)
        self.dec_targets = np.zeros([self.num_data, self.dec_max_len], dtype=np.uint16)
        self.dec_seq_len = np.zeros([self.num_data], dtype=np.uint16)

        for i, p in enumerate(new_pairs):
            char_seq = list(p.ch_sent)
            seq_len = len(char_seq)
            # ch_ids = list(map(lambda x: self.ch_word2id.get(x, self.ch_word2id[UNK]), char_seq))
            ch_ids = seq2ids(char_seq, self.ch_word2id)
            self.enc_inputs[i, :seq_len] = ch_ids
            self.enc_seq_len[i] = seq_len

            # kr_ids = list(map(lambda x: self.kr_word2id.get(x, self.kr_word2id[UNK]), p.kr_tok))
            kr_ids = seq2ids(p.kr_tok, self.kr_word2id)
            self.dec_inputs[i, :p.kr_seq_len+1] = [self.kr_word2id[GO]] + kr_ids
            self.dec_targets[i, :p.kr_seq_len+1] = kr_ids + [self.kr_word2id[EOS]]
            self.dec_seq_len[i] = p.kr_seq_len + 1


class DataSet3(BaseDataSet):
    """
    Encoder Input: Hybrid Chinese tokens and characters
                    tokens + summation of characters in tokens
    Decoder Input: Sequence of Korean Tokens
    """
    def __init__(self,
                 enc_max_len: int,
                 char_max_len: int,
                 dec_max_len: int,
                 ch_word2id: Dict,
                 ch_char2id: Dict,
                 kr_word2id: Dict):
        super().__init__(enc_max_len, dec_max_len, ch_word2id, kr_word2id)
        self.ch_char2id = ch_char2id
        self.enc_sub_inputs = None
        self.char_max_len = char_max_len
        return

    def make(self,
             pairs: List[PairData]):

        new_pairs = list()
        for p in pairs:
            # GO or EOS token will be appended to Korean sequence
            if p.ch_seq_len <= self.enc_max_len and p.kr_seq_len+1 <= self.dec_max_len:
                if p.ch_seq_len < 1 or len(list(p.ch_sent)) < 1 or p.kr_seq_len < 1:
                    pass
                else:
                    new_pairs.append(p)
        del pairs

        self.num_data = len(new_pairs)
        self.full_indices = list(range(self.num_data))
        self.enc_inputs = np.zeros([self.num_data, self.enc_max_len], dtype=np.uint16)
        self.enc_sub_inputs = np.zeros([self.num_data, self.enc_max_len, self.char_max_len], dtype=np.uint16)
        self.enc_seq_len = np.zeros([self.num_data], dtype=np.uint16)

        self.dec_inputs = np.zeros([self.num_data, self.dec_max_len], dtype=np.uint16)
        self.dec_targets = np.zeros([self.num_data, self.dec_max_len], dtype=np.uint16)
        self.dec_seq_len = np.zeros([self.num_data], dtype=np.uint16)

        for i, p in enumerate(new_pairs):
            # ch_ids = list(map(lambda x: self.ch_word2id.get(x, self.ch_word2id[UNK]), p.ch_tok))
            ch_ids = seq2ids(p.ch_tok, self.ch_word2id)
            self.enc_inputs[i, :p.ch_seq_len] = ch_ids

            for j, token in enumerate(p.ch_tok):
                chars = list(token)[:self.char_max_len]
                self.enc_sub_inputs[i, j, :len(chars)] = seq2ids(chars, self.ch_char2id)

            self.enc_seq_len[i] = p.ch_seq_len

            # kr_ids = list(map(lambda x: self.kr_word2id.get(x, self.kr_word2id[UNK]), p.kr_tok))
            kr_ids = seq2ids(p.kr_tok, self.kr_word2id)
            self.dec_inputs[i, :p.kr_seq_len+1] = [self.kr_word2id[GO]] + kr_ids
            self.dec_targets[i, :p.kr_seq_len+1] = kr_ids + [self.kr_word2id[EOS]]
            self.dec_seq_len[i] = p.kr_seq_len + 1

    def get_batch(self, batch_size) \
            -> Tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray, np.ndarray, np.ndarray]:
        indices = random.sample(self.full_indices, batch_size)
        out = (self.enc_inputs[indices, :],
               self.enc_sub_inputs[indices, :, :],
               self.enc_seq_len[indices],
               self.dec_inputs[indices, :],
               self.dec_targets[indices, :],
               self.dec_seq_len[indices])
        return out

    def get_sequential_batch(self, batch_size) -> \
            Tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray, np.ndarray, np.ndarray]:
        s = self.sequential_indices
        e = self.sequential_indices + batch_size
        # If remain data is not enough, < batch size, drop remain data, finish sequential batch
        out = (self.enc_inputs[s:e, :],
               self.enc_sub_inputs[s:e, :, :],
               self.enc_seq_len[s:e],
               self.dec_inputs[s:e, :],
               self.dec_targets[s:e, :],
               self.dec_seq_len[s:e])
        self.sequential_indices += batch_size
        return out


def pickle_store(obj, path):
    f = open(path, 'wb')
    pickle.dump(obj, f)
    f.close()
    return


def pickle_load(path):
    f = open(path, 'rb')
    a = pickle.load(f)
    f.close()
    return a


def print_write(s, file, mode=None):
    if isinstance(file, str):
        if mode is None:
            mode = 'a'
        f = open(file, mode)
        print(s, end='')
        f.write(s)
        f.close()
    else:
        print(s, end='')
        file.write(s)


def chinese_regularize(line, dic):
    """
    Change all chinese character unified
    :param line: Chinese or Korean that has chinese character sentence
    :param dic: compatibility -> unified chinese character dictionary
    :return: unified chinese sentence
    """
    line_out = ''
    for c in line:
        try:
            line_out = line_out + dic[c]
        except KeyError:
            line_out = line_out + c
    return line_out


def read_dic(path, start_line):
    f = open(path, 'r', encoding='utf-8')
    lines = f.readlines()[start_line:]
    f.close()
    dic = dict()
    for l in lines:
        s = l.split()
        dic[s[0]] = s[1]
    return dic


def pronounce(ne, p, pi):
    h = pi[ne[0]]
    for i, c in enumerate(ne[1:]):
        h = h + p[c]
    return h


def read_xlsx(xlsx_path, column: List):
    wb = openpyxl.load_workbook(xlsx_path)
    ws = wb.active
    data = []
    if len(column) == 1:
        for r in ws.rows:
            data.append(r[column[0]].value)
    else:
        for r in ws.rows:
            rr = []
            for c in column:
                rr.append(r[c].value)
            data.append(tuple(rr))
    wb.close()
    return data


def dict_swap(my_dic: Dict):
    return {v: k for k, v in my_dic.items()}


def uni2dec(c):
    encoded = list(c.encode('unicode-escape'))[2:]
    if not encoded:
        return 0
    h = ''
    for c in encoded:
        h += chr(c)
    return int(h, 16)


def unicode_classify(c):
    dec = uni2dec(c)
    if (int('4E00', 16) <= dec <= int('9FFF', 16) or
            int('3400', 16) <= dec <= int('4DBF', 16) or
            int('F900', 16) <= dec <= int('FAFF', 16)):
        return 0    # Chinese
    elif int('AC00', 16) <= dec <= int('D7AF', 16):
        return 1    # Korean
    else:
        return 2    # else


def seq2ids(seq: List[str], d: Dict) -> List[int]:
    ids = list(map(lambda x: d.get(x, d[UNK]), seq))
    return ids


def ids2seq(ids: Union[np.ndarray, List[int]], d: Dict, clip_token: Optional[str] = None):
    seq = list(map(lambda x: d[x], ids))
    if clip_token is not None:
        try:
            return seq[:seq.index(clip_token)]
        except ValueError:
            return seq
    else:
        return seq


def load_word2vec(model: Union[Word2Vec, str],
                  special_words: List[str]) -> Tuple[Dict, np.ndarray, int]:
    """
    :param model: gensim word2vec model
    :param special_words: List of special word such as Pad, Unknown, Go and EOS token
    :return: word dictionary, embedding vectors, embedding size
    """
    if isinstance(model, str):
        model = Word2Vec.load(model)

    size = model.wv.vectors.shape[1]

    special_vectors = list()
    m = np.mean(model.wv.vectors)
    s = np.std(model.wv.vectors)
    for word in special_words:
        if word == PAD:
            special_vectors.append(np.zeros((1, size)))
        else:
            special_vectors.append(np.random.normal(size=(1, size), loc=m, scale=s))

    special_vectors = np.concatenate(special_vectors, axis=0)
    assert special_vectors.shape[0] == len(special_words)

    word2idx = {c: i for i, c in enumerate(special_words + model.wv.index2word)}
    vectors = np.concatenate((special_vectors, model.wv.vectors), axis=0)

    return word2idx, vectors, size


def detokenize(tokens: List[str], mecab):
    tagged = mecab.pos(''.join(tokens))
    # print(tagged)
    out = ''
    for word, tag in tagged:
        out += word
        # J-조사, E-어미, M-부사
        if 'J' in tag or 'E' in tag or 'M' in tag:
            out += ' '
    return out
