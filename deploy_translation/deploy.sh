#!/usr/bin/env bash
nvidia-docker stop translation_container
nvidia-docker rm translation_container
nvidia-docker build --network host -t translation_image .
nvidia-docker run -d -p 50053:50053 --name translation_container translation_image
