from model_NER import *
from model_chunking import *

"""
running chunking and named entity recognition
tensorflow 1.9
Do not change 'chunking folder' and 'NER folder'
"""


ck_graph = tf.Graph()                                           # make chunking graph
ck_sess = tf.Session(graph=ck_graph, config=get_tf_config())    # make chunking session for chunking graph
ck_ch2idx = pickle_load('chunking/ch2idx.p')                    # chinese to index for chunking

with ck_graph.as_default():                                     # build chunking model in chunking graph
    ck_model = RnnChunking('chunking', 2, 512, 300, ck_ch2idx, MARK2IDX, MARK_IDX2MARK_NAME)
    ck_model.build()
    ck_model.init_model(ck_sess, ckpt='weights-4000')           # load chunking model parameters


ner_graph = tf.Graph()                                          # make NER graph
ner_sess = tf.Session(graph=ner_graph, config=get_tf_config())  # make NER session for NER graph
ner_ch2idx = pickle_load('NER/ch2idx.p')                        # chinese to index for NER

with ner_graph.as_default():                                    # build NER model in NER graph
    ner_model = RnnNER('NER', 2, 512, 600, ner_ch2idx, BME2IDX)
    ner_model.build()
    ner_model.init_model(ner_sess, ckpt='weights-5000')         # load NER model parameters


sentence = '宣傳官羅廷彦還自慶源書啓北方事賊胡悔禍有乞降之意云云藩胡等刷還慶源判官梁思毅妾及婢子二名'
# sentence = '宣傳官羅廷彦還自慶源。書啓北方事。賊胡悔禍有乞降之意云云。藩胡等刷還慶源判官梁思毅妾及婢子二名。'
# 선전관(宣傳官) 나정언(羅廷彦)이 경원(慶源)에서 돌아와 북방(北方)의 일을 서계(書啓)하였는데,
# “적호(賊胡)가 재난을 일으킨 것을 후회하고[悔禍] 항복을 구걸할 뜻이 있고, 번호(藩胡)들이
# 경원 판관(慶源判官) 양사의(梁思毅)의 첩(妾)과 여종 2명을 쇄환(刷還)하였다.”고 하였다.


ck_out = ck_model.run(ck_sess, sentence, rm_mark=False)         # Add sentence mark for chinese sentence.
ner_out = ner_model.run(ner_sess, ck_out)                       # output -> list[(entity, index of entity)]

ck_sess.close()
ner_sess.close()

print(sentence)
print(ck_out)
print(ner_out)
