import tensorflow as tf
import openpyxl
import numpy as np
import pickle
from typing import Union, Optional, Dict, List, Tuple
import random
import os
from copy import deepcopy


BUCKET = np.arange(5.1, 101, 5)
MARK2IDX = {'N': 0, '。': 1, ' ': 2}
MARK_IDX2MARK_NAME = {0: 'None', 1: 'Dot', 2: 'Space'}
MARK = ['。', ' ']


class LineData:
    def __init__(self,
                 ch_line: str,
                 kr_line: str,
                 x: np.ndarray,
                 y: np.ndarray,
                 loss_wt: np.ndarray,
                 seq_len: int,
                 whole_len: int):
        self.ch_line = ch_line
        self.kr_line = kr_line
        self.x = x
        self.y = y
        self.loss_wt = loss_wt
        self.seq_len = seq_len
        self.whole_len = whole_len


class BatchData:
    """
    batch data of LineData
    """
    def __init__(self, samples: List[LineData]):
        """
        :param samples: List of LineData
        """
        self.size = len(samples)
        whole_len_b = np.unique([sample.whole_len for sample in samples])
        if len(whole_len_b) != 1:
            raise ValueError
        else:
            self.length = int(whole_len_b[0])
        self.ch_line_b = []
        self.kr_line_b = []
        self.x_b = np.zeros((self.size, self.length), np.int16)
        self.y_b = np.zeros((self.size, self.length), np.int16)
        self.loss_wt_b = np.zeros((self.size, self.length), np.float16)
        self.seq_len_b = np.zeros(self.size, np.int16)
        for i, sample in enumerate(samples):
            self.ch_line_b.append(sample.ch_line)
            self.kr_line_b.append(sample.kr_line)
            self.x_b[i, :] = sample.x
            self.y_b[i, :] = sample.y
            self.loss_wt_b[i, :] = sample.loss_wt
            self.seq_len_b[i] = sample.seq_len


def pickle_store(obj, path):
    f = open(path, 'wb')
    pickle.dump(obj, f)
    f.close()
    return


def pickle_load(path):
    f = open(path, 'rb')
    a = pickle.load(f)
    f.close()
    return a


def read_data(folder):
    files = os.listdir(folder)
    d = []
    for f in files:
        if f.split('.')[-1] == 'p':
            d = d + pickle_load(os.path.join(folder, f))
    return d


def uni2dec(c):
    encoded = list(c.encode('unicode-escape'))[2:]
    if not encoded:
        return 0
    h = ''
    for c in encoded:
        h += chr(c)
    return int(h, 16)


def unicode_classify(c) -> int:
    dec = uni2dec(c)
    if (int('4E00', 16) <= dec <= int('9FFF', 16) or
            int('3400', 16) <= dec <= int('4DBF', 16) or
            int('F900', 16) <= dec <= int('FAFF', 16)):
        return 0    # Chinese
    elif int('AC00', 16) <= dec <= int('D7AF', 16):
        return 1    # Korean
    else:
        return 2    # else


def remove_redundant_line(lines, check=0):
    new_lines = []
    for line in lines:
        f = False
        for c in line:
            if unicode_classify(c) == check:    # 0 hanja, 1 hangeul
                f = True
                break
        if f:
            new_lines.append(line)
    return new_lines


def merge_line(lines, num_merge=3):
    if isinstance(lines[0], str):
        new_lines = []
        for i in range(len(lines)-num_merge):
            temp = ''
            for j in range(num_merge):
                line = lines[i + j].replace('。', ' ').rstrip()
                if line[-1] != '。':
                    temp = temp + line + '。'
                else:
                    temp = temp + line
            new_lines.append(temp + '\n')
    elif isinstance(lines[0], list):
        new_lines = []
        for i in range(len(lines)-num_merge):
            temp = ['', '']
            for j in range(num_merge):
                ch = lines[i+j][0].replace('。', ' ').rstrip()
                kr = lines[i+j][1].rstrip()
                if ch[-1] != '。':
                    temp[0] = temp[0] + ch + '。'
                else:
                    temp[0] = temp[0] + ch
                temp[1] = temp[1] + kr + ' '
            new_lines.append(temp)
    else:
        raise ValueError
    return new_lines


def line2data(line: str, ch2idx: Dict, mark2idx: Dict):
    seq = []
    mark = list(mark2idx.keys())
    for c in line:
        if mark.count(c) == 0:
            seq.append(c)
        else:
            if len(seq) != 0:
                seq[-1] = seq[-1] + c
    length = len(seq)
    x = np.zeros(length, dtype=np.int16)
    y = np.zeros(length, dtype=np.int16)
    wt = np.zeros(length, dtype=np.float16)
    for i, c in enumerate(seq):
        if len(c) == 1:
            try:
                x[i] = ch2idx[c]
            except KeyError:
                x[i] = ch2idx['U']
            y[i] = mark2idx['N']
            if c == 'P':
                wt[i] = 0
            else:
                wt[i] = 1
        else:
            try:
                x[i] = ch2idx[c[0]]
            except KeyError:
                x[i] = ch2idx['U']
            try:
                y[i] = mark2idx[c[1:]]
            except KeyError:
                y[i] = mark2idx[' ']
            if c[0] == 'P':
                wt[i] = 0
            else:
                wt[i] = 1
    try:
        seq_len = seq.index('P')
    except ValueError:
        seq_len = length
    return x, y, wt, seq_len


def pad_line(line, bucket):
    length = len(remove_mark(line))
    temp = np.append(bucket, [length])
    temp.sort()
    kk = np.where(temp == float(length))[0][0]
    if kk < bucket.shape[0]:
        padded = line + 'P' * (int(bucket[kk]) - length)
    else:
        seq = []
        for c in line:
            if MARK.count(c) == 0:
                seq.append(c)
            else:
                if len(seq) != 0:
                    seq[-1] = seq[-1] + c
        seq = seq[:int(bucket[-1])]
        padded = ''.join(seq)
    return padded


def line_split(line: str, max_len: int, del_redundant=True):
    splitted = []
    p = list(range(0, len(line), max_len//2))
    for i in range(len(p)):
        try:
            splitted.append(line[p[i]:p[i+2]])
        except IndexError:
            splitted.append(line[p[i]:])
            break
    if del_redundant:
        return remove_redundant_line(splitted)
    else:
        return splitted


def bucketting(data: List, bucket: Union[List, np.ndarray]) -> Tuple[List[List[LineData]], np.ndarray]:
    """
    Bucket list of LineData as specified bucket
    :param data: List of Line Data
    :param bucket: max length list of sequences
    :return: bucketted data & the probability each bucket selected
    """
    bucketted = []
    for i in range(bucket.shape[0]):
        bucketted.append([])

    for d in data:
        seq_len = d.seq_len
        temp = np.append(bucket, [seq_len])
        temp.sort()
        kk = np.where(temp == float(seq_len))[0][0]
        if kk < bucket.shape[0]:
            bucketted[kk].append(d)

    ndata = len(data)
    nbucket = len(bucketted)
    bucket_prob = np.zeros(nbucket)
    for i in range(nbucket):
        bucket_prob[i] = len(bucketted[i]) / ndata
    return bucketted, bucket_prob


def get_batch(bucketted: List[List[LineData]], bucket_prob: np.ndarray, batch_size: int) -> BatchData:
    """
    Get batch data using bucketted LineData
    :param bucketted: bucketted LineData (number of bucket, number of data in the bucket)
    :param bucket_prob: The probability that the bucket will be selected.
                        according to the number of data in the bucket.
                        the number of data in the bucket is increase, the probability the bucket selected is increase.
    :param batch_size: the number of data in the batch
    :return: BatchData
    """
    t = np.random.choice(len(bucketted), 1, False, bucket_prob)[0]
    try:
        samples = random.sample(bucketted[t], batch_size)
    except ValueError:
        samples = random.sample(bucketted[t], len(bucketted[t]))
    return BatchData(samples)


def lstm_cell(hidden, keep_prob):
    cell = tf.nn.rnn_cell.BasicLSTMCell(hidden, state_is_tuple=True)
    cell = tf.nn.rnn_cell.DropoutWrapper(cell, output_keep_prob=keep_prob)
    return cell


def per_class_acc(label, prediction, num_class, scope):
    with tf.variable_scope(scope):
        _, op = tf.metrics.mean_per_class_accuracy(labels=label, predictions=prediction, num_classes=num_class)
    local_val = tf.get_collection(tf.GraphKeys.LOCAL_VARIABLES, scope=scope)[0]
    acc = tf.split(tf.div(tf.diag_part(local_val), tf.reduce_sum(local_val, 1)), num_class)
    return acc, op


def get_class_acc_print(class_acc, idx2mark):
    _format = ''
    _arg = []
    for i in range(len(idx2mark)):
        _format = _format + '%s->%0.3f, '
        if len(class_acc[i]) == 1:
            _arg = _arg + [idx2mark[i], class_acc[i]]
        else:
            acc = class_acc[i, i] / (np.sum(class_acc[i])+1e-10)
            _arg = _arg + [idx2mark[i], acc]
    return _format, _arg


def get_tf_config():
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    return config


def print_write(s, file, mode=None):
    if isinstance(file, str):
        if mode is None:
            mode = 'a'
        f = open(file, mode)
        print(s, end='')
        f.write(s)
        f.close()
    else:
        print(s, end='')
        file.write(s)


def dict_swap(my_dic: Dict):
    return {v: k for k, v in my_dic.items()}


def text_readlines(file_path: str) -> List[str]:
    f = open(file_path, 'r', encoding='utf-8')
    t = f.readlines()
    f.close()
    return t


def text_read(file_path: str) -> str:
    f = open(file_path, 'r', encoding='utf-8')
    t = f.read()
    f.close()
    return t


def read_dic(path: str) -> Dict:
    f = open(path, 'r', encoding='utf-8')
    lines = f.readlines()[2:]
    f.close()
    dic = {}
    for l in lines:
        s = l.split()
        dic[s[0]] = s[1]
    return dic


def chinese_regularizer(line, dic):
    """
    Change all chinese character unified
    :param line: Chinese or Korean that has chinese character sentence
    :param dic: compatibility -> unified chinese character dictionary
    :return: unified chinese sentence
    """
    line_out = ''
    for c in line:
        try:
            line_out = line_out + dic[c]
        except KeyError:
            line_out = line_out + c
    return line_out


def read_xlsx(xlsx_path, column):
    wb = openpyxl.load_workbook(xlsx_path)
    ws = wb.active
    data = []
    if len(column) == 1:
        for r in ws.rows:
            data.append(r[column[0]].value)
    else:
        for r in ws.rows:
            rr = []
            for c in column:
                rr.append(r[c].value)
            data.append(rr)
    wb.close()
    return data


def sentence2input(sentence: str, ch2idx: Dict, rm_mark=False):
    if rm_mark:
        removed = remove_mark(sentence)
    else:
        removed = deepcopy(sentence)
    x = []
    for c in removed:
        try:
            x.append(ch2idx[c])
        except KeyError:
            x.append(ch2idx['U'])
    return x, removed


def remove_mark(line):
    out = ''
    for c in line:
        if MARK.count(c) == 0:
            out = out + c
    return out
