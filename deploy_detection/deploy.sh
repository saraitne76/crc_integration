#!/usr/bin/env bash
nvidia-docker stop detection_container
nvidia-docker rm detection_container
nvidia-docker build --network host -t detection_image .
nvidia-docker run -d -p 50051:50051 --name detection_container detection_image
