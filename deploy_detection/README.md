# About Detection module  

## Input  
- The detector model uses 1400x1400 as the input size. (If another size comes in, I forced it to resize it.)  
- Detection is good for sizes larger than 600 * 600, but not for sizes smaller than 600 * 600.  

## Output  
- result = {
	'origin': origianl input image
	'boxed': detected boxes drawn image
	'coord': coordinates of boxes (ordered)
	}
