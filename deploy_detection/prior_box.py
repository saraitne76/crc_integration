import torch
from math import sqrt as sqrt
from itertools import product as product
from box_utils import point_form, center_size

class PriorBox(object):
    def __init__(self, feature, img_size, which_gpu):
        super(PriorBox, self).__init__()

        # feature map from SSD Network
        self.feature_maps = feature

        _, _, self.image_h, self.image_w = img_size

        self.min_dim = min(self.image_h, self.image_w)
        self.max_dim = max(self.image_h, self.image_w)

        self.medium_dim = sqrt(self.image_h * self.image_w)

        # For default boxes scale, it means scale factor
        self.min_sizes = [2]
        self.max_sizes = [600]

        self.aspect_ratios = [list(range(2,11))]
        
        self.clip = True
    
    def forward(self):
        pbox = []
        # Loop each Feature map
        for k, feature in enumerate(self.feature_maps):
            _, _, fh, fw = feature

            scale = self.min_sizes[k] / self.medium_dim
            scale_prime = sqrt(scale * (self.max_sizes[k] / self.medium_dim))

            # Loop each axis
            for y, x in product(range(fh), range(fw)):
                # Unit center x,y
                cx = (x + 0.5) / fw
                cy = (y + 0.5) / fh
                pbox += [cx, cy, scale, scale]
                pbox += [cx, cy, scale_prime, scale_prime]
    
                for ar in self.aspect_ratios[k]:
                    pbox += [cx, cy, scale * sqrt(ar), scale / sqrt(ar)]                        
                    pbox += [cx, cy, scale / sqrt(ar), scale * sqrt(ar)]

        # [num_priors , 4]
        output = torch.DoubleTensor(pbox).view(-1, 4)

        if self.clip:
            output = point_form(output)
            output.clamp_(max=1, min=0)
            output = center_size(output)
        
        return output
