# for Pytorch

torch==0.4.1
torchvision==0.2.2
matplotlib
opencv-python

absl-py==0.4.1
aniso8601==3.0.2
appnope==0.1.0
astor==0.7.1
backcall==0.1.0
certifi==2018.8.24
chardet==3.0.4
click==6.7
decorator==4.3.0
Flask==1.0.2
Flask-RESTful==0.3.6
gast==0.2.0
#get==1.0.3
#grpcio==1.12.1
idna==2.7
#ipdb==0.11
#ipython==6.5.0
#ipython-genutils==0.2.0
itsdangerous==0.24
jedi==0.12.1
Jinja2==2.10
Markdown==2.6.11
MarkupSafe==1.0
#mkl-fft==1.0.4
#mkl-random==1.0.1
numpy==1.15.1
parso==0.3.1
pexpect==4.6.0
pickleshare==0.7.4

#Pillow==5.2.0
Pillow

#post==1.0.2
prompt-toolkit==1.0.15
protobuf==3.6.0
ptyprocess==0.6.0
#public==1.0.3
Pygments==2.2.0
pytz==2018.5
#query-string==1.0.2
requests==2.19.1
simplegeneric==0.8.1
six==1.11.0
termcolor==1.1.0
traitlets==4.3.2
urllib3==1.23
wcwidth==0.1.7
Werkzeug==0.14.1
