from flask import Flask
from flask_restful import Resource, Api, reqparse
from model import Model
import werkzeug
from PIL import Image
import numpy as np
import os

os.environ['CUDA_VISIBLE_DEVICES'] = '0'

parent_path = os.path.dirname(os.getcwd())
parser = reqparse.RequestParser()
parser.add_argument('input', type=werkzeug.datastructures.FileStorage, location='files')
parser.add_argument('trained_model', default='crc_integration_default_weight.model', type=str,
                        help='Trained state_dict file path to open')
parser.add_argument('top_k', default=1000, type=int,
                        help='Further restrict the number of predictions to parse (int)')
parser.add_argument('cuda', default=True,
                        help='Use cuda to train model (bool)')
parser.add_argument('show_number', default=True, type=str,
                        help='Determine show or not numbering result (str)')
parser.add_argument('gpu', default=0, type=int,
                        help='Select GPU number (int)')
parser.add_argument('num_dbox', default=20, type=int,
                        help='Select number of default boxes (int)')
parser.add_argument('num_classes', default=2, type=int,
                        help='Select number of classes (int)')
parser.add_argument('score_thresh', default=0.1, type=float,
                        help='Select score threshold value (float)')
parser.add_argument('nms_thresh', default=0.5, type=float,
                        help='Select NMS threshold value (float)')
parser.add_argument('mode', default='imshow', type=str,
                        help='Determine show image or performance check (str)')
parser.add_argument('dataset_name', default='caoshu', type=str,
                        help='Determine which dataset use (str)')
parser.add_argument('image_path', default='result/images/', type=str,
                        help='Determine where save result path (str)')
parser.add_argument('graph_path', default='result/graph/', type=str,
                       help='Determine where save result path (str)')
parser.add_argument('model_path', default='result/weights/', type=str,
                        help='Where is the saved model path (str)')



app = Flask(__name__)
api = Api(app)

# test input
load_input = parent_path + '/재영남일기.jpg'

class Service(Resource):
    def __init__(self):
        self.args = parser.parse_args(strict=True)
        self.model = Model(self.args)

    def post(self):
        # Run the model by code input
        #return self.model.run(input=load_input)

        # Run the model by args input
        return self.model.run(input=self.args['input'])

# /{...} : Enter service name in {...}
api.add_resource(Service, '/detection')

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=50051)
