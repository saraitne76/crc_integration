# coding: utf-8
import matplotlib.pyplot as plot
import matplotlib.patches as patches
import matplotlib.patches as Rectangle
from PIL import Image
import os, cv2
import csv
import math
import numpy as np
import copy
import operator

def get_np_list(CSV_path):#csv_path = "/home/ihs/Documents/프로젝트/hanja_project/CRC_data/임시소팅/prediction.csv"
    file = open(CSV_path,'r') 
    data = np.array([[0.0,0.0,0.0,0.0]])  #initialize
    with file:
        reader = csv.reader(file, delimiter = "\t") 
        for k,row in enumerate(reader): #row  = ['1122.00769042969,284.650451660156,1189.92333984375,351.471374511719']
            
            row_sep = row[0].split(',')
            if k ==0 : 
                data[0] = list(map(float,row_sep))
                continue
                
            data = np.append(data, [ list(map(float,row_sep))], axis = 0)
    file.close()
    return data

#MakelistNP 검출된 박스를 넘파이 배열을 받아서  sorting 된 넘파이 배열로 내보냄
class MakeListNP():

    def __init__(self, input_img_np, img_shape):   #input_img_np  : type은 numpy 일 것 !  인풋 1 : 박스 좌표들 넘파이 , 인풋 2 : 이미지 shape
        self.img_pre_list = input_img_np.tolist()           #img_pre_list : 인풋 넘파이를 리스트로 완전 변환. pre =prediction
        self.img_h = img_shape[0]
        
    def make_area_xywh(self):  # self.area_xywh_list 를 만든다. 리스트 안의 원소는 다음의 형태를 띈다. [박스크기, [x,y,w,h]]
        self.area_xywh_list = []
        for k,xyxy in enumerate(self.img_pre_list):   #xyxy  = [x,y,x,y] 의 형태로 박스 좌상단, 우하단의 좌표를 가리킨다.
            self.x1, self.y1, self.x2, self.y2 = xyxy
            self.w = self.x2 - self.x1
            self.h  = self.y2 - self.y1
            self.quadrangle = (self.x1+self.w)*(self.img_h - self.y1)   # (x1+w) * (img_height - y1)
            self.area_xywh_list.append([int(self.quadrangle), [int(self.x1), int(self.y1), int(self.w), int(self.h)]])
    
    def make_group(self):  #gp_list, area_xywh_list, group 사용
        
        if self.group == 1 :
            self.area_xywh_list = sorted(self.area_xywh_list, reverse= True)
            self.group = 0 
            
        self.mid = self.area_xywh_list[0][1][0] + self.area_xywh_list[0][1][2]/2    #[0][1][0] = x , [0][1][2] = w
        self.y_cut = self.area_xywh_list[0][1][1]
        self.w_length = self.area_xywh_list[0][1][2]
        self.de_list = []
        self.cand_list = [] 
        self.group_list.append(self.area_xywh_list[0][1])
        
        if len(self.area_xywh_list) == 1 :
            self.group = 1
            return self.group_list, self.cand_list, self.group
        else : 
            self.area_xywh_list = self.area_xywh_list[1:] 
        

        for k,j in enumerate(self.area_xywh_list):
            self.c_mid = j[1][0] + j[1][2]/2
            self.c_w_length = j[1][2]
            self.midgap = abs(self.mid - self.c_mid)
            if ( (j[1][0]+j[1][2])> self.mid and j[1][0] < self.mid and self.c_w_length < self.w_length*0.6 and self.y_cut < j[1][1]) or (self.midgap < j[1][2]*0.4 and self.y_cut < j[1][1]) : 
                self.de_list.append(j)
            else :
                self.cand_list.append(j)

        if len(self.de_list) == 0 : 
            self.group = 1
            return self.group_list, self.cand_list, self.group
        else :
            self.de_list = sorted(self.de_list , key = lambda C : C[1][1],  reverse=False)
            
        self.cand_list = self.de_list + self.cand_list

        return self.group_list , self.cand_list, self.group

    def numbering(self):   #

        self.make_area_xywh() #내부 메소드 호출. 각 요소들을  [사각형 크기, [ x,y,w,h 좌표], ...] 의 형태로 self.area_xywh_list 에 반환
        self.group_dict = {} #dict 형태로 key 와 넘버링 라인별 묶음을 value로 저장
        self.dict_num = 0   #key 넘버를 0부터 주기 위한 것 
        self.group_list = []
        self.group = 1
        
        while True:
        
            self.group_list , self.area_xywh_list, self.group = self.make_group() #내부 메소드 호출.  self.area_xywh_list = self.not_group_list 
            if self.group == 1 :
                self.group_dict[self.dict_num] = sorted(self.group_list , key = lambda C : C[1], reverse=False)
                self.group_list = []
                self.dict_num += 1
            if len(self.area_xywh_list) == 0 :
                break
        #print("key 값과 value 들  = " ,self.group_dict)
        #-------------------------------------------------------------------------------------------------------------------------------------------------------------
        self.order_key_list = []  # 무리 그룹들 각각의 x값을 기준으로 다시 재정렬한 key 값을 모은 리스트. (ex : key 넘버가 0, 1, 2, 순서대로 일때 x값 기준으로 key 들을 재정렬 하면 0, 16, 1 등...
        for j in sorted(self.group_dict.items(), key =operator.itemgetter(1) , reverse=True):
            self.order_key_list.append(j[0])
        #------------------------------------------------------------실제 순서대로 반환해 주기 --------------------------------------------------------------------
        self.true_order_boxes =[]
        for j in self.order_key_list:  # j  = key number 
            for i,xyxy in enumerate(self.group_dict[j]):
                self.true_order_boxes.append(xyxy)
                
    def get_boxes_numbered(self):
        
        for k,xywh in enumerate(self.true_order_boxes) :
            self.x1, self.y1, self.x2, self.y2 = xywh[0], xywh[1], xywh[2] + xywh[0],   xywh[3] + xywh[1]
            self.true_order_boxes[k] = [self.x1, self.y1, self.x2, self.y2]
        return np.array(self.true_order_boxes) 

#csv_path = "/home/ihsong/Documents/프로젝트/hanja_project/CRC_data/임시소팅/prediction.csv"
#prediction_data = get_np_list(csv_path)
#img_shape = (1400,1400,3)

#MLNP = MakeListNP(prediction_data, img_shape)
#MLNP.numbering()
#numbered_boxes = MLNP.get_boxes_numbered()

#print(numbered_boxes)
