import json
import numpy as np
import torch
import matplotlib
matplotlib.use('Agg')
import sys
from ssd import build_ssd
from data_loader import image_loader
from visualize import plot_pred_boxes_with_return
from ordering import MakeListNP 
from PIL import Image
import cv2
from io import BytesIO
import base64
from PIL import Image, ImageDraw, ImageFont


######################################################################################################################
# Lee Jangwon's codes
# Modified by Junhwan Ryu (in draw_box function)
######################################################################################################################
def img2str(img):
    """
    :param img: PIL Image object
    :return: byte string
    """
    byte_array = BytesIO()
    img.save(byte_array, format='JPEG')
    byte_array = base64.b64encode(byte_array.getvalue())
    # 'byte array' to 'str' for json dumps
    byte_to_str = byte_array.decode("utf-8")
    return byte_to_str


def str2img(img_str):
    """
    :param img_str: byte string image
    :return: PIL Image object
    """
    i = BytesIO(base64.b64decode(img_str))
    return Image.open(i)


def draw_box(img, coord, args):
    """
    Draw Bounding Box on PIL Image object using predicted box coordinates
    :param img: PIL Image object
    :param coord: List of coordinate - List[(x1, y1, x2, y2), (int, int, int, int)]
    :return: PIL Image object boxes drawn
    """
    img_drawn = img.copy()
    draw = ImageDraw.Draw(img_drawn)
    # font = ImageFont.truetype("/usr/share/fonts/truetype/porson/GFSPorson.otf", 30)	
    font = ImageFont.truetype("GFSPorson.otf", 30)

    for cnt, (x1, y1, x2, y2) in enumerate(coord):
        draw.line([(x1, y1), (x2, y1), (x2, y2), (x1, y2), (x1, y1)], width=4, fill="blue")
        # Showing numbering results.
        if args.show_number:
            draw.text(( int((x1+x2)/2), int((y1+y2)/2) ), str(cnt), font=font)

    return img_drawn
######################################################################################################################
######################################################################################################################
######################################################################################################################


class Model(object):
    def __init__(self, args):
        # Set hyper Parameters
        self.args = args
        # Build network
        self.network = build_ssd('test', self.args.num_classes, self.args.nms_thresh, self.args.num_dbox, self.args.gpu)
        # Init network
        self.init_network()

    def init_network(self):
        self.network.load_state_dict(torch.load(self.args.model_path + self.args.trained_model))
        self.device = 'cuda:' + str(self.args.gpu)
        print('Loading pre-trained models is over.')

    def get_detected_boxes(self, img):
        _, _, h, w = img.size()

        # Inference
        detections = self.network(img).data
        if detections.dim() == 4:
            dets = detections[0, 1, :, :]
        else:
            dets = detections

        mask = dets[:, 0].gt(self.args.score_thresh).expand(5, dets.size(0)).t()
        dets = torch.masked_select(detections, mask).view(-1, 5)

        if dets.size(0) == 0:
            print('zero detection')
            return 

        preds = dets[:, 1:]
        preds[:, 0] *= w
        preds[:, 2] *= w
        preds[:, 1] *= h
        preds[:, 3] *= h

        return preds

    def get_detected_images(self, img, preds, iname): 
        img = plot_pred_boxes_with_return(img, preds, self.args, iname)

        # Image to byte array
        # from io import BytesIO
        # import base64
        byte_array = BytesIO()
        img.save(byte_array, format='JPEG')
        byte_array = base64.b64encode(byte_array.getvalue())
        # 'byte array' to 'str' for json dumps
        byte_to_str = byte_array.decode("utf-8")

        # Byte array to image reconstruction test is okay.
        #recons_byte_array = bytes(byte_to_str, encoding='utf-8')
        #from PIL import Image
        #recons_image = Image.open(BytesIO(byte_array))
        #plot_pred_boxes_with_return(recons_image, preds, self.args)
        return byte_to_str

    def get_cropped_images(self, img, preds):
        cropped_list = []
        # Specific crop size by KNU.
        crop_size = (224, 224)
        for pred in preds.astype(int):
            x1, y1, x2, y2 = pred
            cropped_list.append(cv2.resize(img[y1:y2, x1:x2, :], crop_size))
        cropped_imgs = np.asarray(cropped_list)
        return cropped_imgs

    def run(self, input):

        with torch.no_grad():
            # Get data
            img, img_cv2, iname = image_loader(input, 'crc')

            # Assign to gpu.
            if self.args.cuda:
                img = img.to(self.device)
                self.network = self.network.to(self.device)
            self.network.eval()

            # Inference
            preds = self.get_detected_boxes(img)

            # Get image with predicted boxes
            # boxed_img_byte = self.get_detected_images(img_cv2, preds, iname)

            # result example(i.e., Detection)
            preds = preds.cpu().numpy()

            # remove 0 elements
            preds = preds[preds[:, 0] > 0, :]

            # Sorting based on y-axis for convenience of numbering algorithm.
            sorted_preds = preds[preds[:, 1].argsort()]

            # Get ordered boxes (code by Ikhwan Song).
            MLNP= MakeListNP(preds, (1400, 1400, 3))
            MLNP.numbering()
            ordered_boxes = MLNP.get_boxes_numbered()

            # Cropped images (4-D array) => (N, W, H, C)
            # cropped_imgs = self.get_cropped_images(img_cv2, ordered_boxes)

            #######################################################################################################
            # Lee Jangwon's codes
            # Update ordering results variable name for compatible with Junhwan Ryu.
            #######################################################################################################
            preds = np.array(ordered_boxes, dtype=np.uint16).tolist()
            img_box_drawn = draw_box(Image.fromarray(img_cv2), preds, self.args)

            # for ordering result test, in YU.
            img_box_drawn.save(self.args.image_path + 'test.png')

            img_origin = Image.fromarray(img_cv2)
            #######################################################################################################
            #######################################################################################################
            #######################################################################################################
            result = {
                'origin': img2str(img_origin),
                'boxed': img2str(img_box_drawn),
                'coord': preds
            }
        return json.dumps(result)   # return as json string
