import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from PIL import Image


def plot_pred_boxes_with_return(img, preds, args, iname):
    preds = preds.cpu().numpy()

    fig, ax = plt.subplots()
    ax.imshow(img)

    for box in preds:
        x1, y1, x2, y2 = box
        rect = patches.Rectangle((x1,y1), x2-x1, y2-y1, linewidth=1, edgecolor='b', facecolor='none')
        ax.add_patch(rect)
    #rect.set(label='prediction')

    #ax.legend()
    plt.savefig(args.image_path + iname, dpi=300)

    return Image.fromarray(img)
