# RESTful API Server for CRC Project
- Flask 파이썬 RESTful API 서버
- Hosting Server : 155.230.16.52
- Web Service Server : www.dila.co.kr
- Run module : bash deploy.sh
- 검출기 : Input : 원본 이미지 파일(.jpg), Output : 좌표 리스트 in JSON format(string)
- 인식기 : Input : 좌표 리스트 in JSON format(string), Output : 인식된 문장(string)
- 번역기 : Input : 인식된 문장(string), Output : 번역된 문장(string)
    > 번역은 (문장부호예측 기반 문장분할)->(NER)->(NMT) 를 통합

# JSON Web Token(JWT) 발급
- API를 허가된 user 에게만 이용 가능하도록 하기 위해 도입, 매번 다른 token 값이 생성됨
- 요청 시, 발급받은 token을 헤더에 명시해야만 서비스 이용가능
```
# get token from api server...
curl localhost:50000/auth

# Then, you will see...
{"access_token":"b'eyJhbGci...'","msg":"Successfully issued token!"}
```
    
# API 호출(Local Test)
    # Detection Service(POST)
    curl -X POST -H "Content-Type: multipart/form-data" -F "input=@image.jpg" localhost:50051/detection > detection.json

    # Request Recognition Service(POST)
    curl -X POST -H "Content-Type: multipart/form-data" -F "input=@detection.json" localhost:50052/recognition > recognition.json

    # Request Translation Service(POST)
    curl -X POST -H "Content-Type: multipart/form-data" -F "input=@recognition.json" localhost:50053/translation > translation.json

# API 호출
    # NOTE #
    # {token_value} : 발급받은 토큰 값, b''를 제외한 string을 사용함
    
    # Detection Service(POST)
    curl -X POST -H "Content-Type: multipart/form-data" -H "Authorization: {token_value}" -F "detection=@image.jpg" localhost:50000/detection > detection.json

    # Request Recognition Service(POST)
    curl -X POST -H "Content-Type: multipart/form-data" -H "Authorization: {token_value}" -F "recognition=@detection.json" localhost:50000/recognition > recognition.json

    # Request Translation Service(GET)
    curl -X POST -H "Content-Type: multipart/form-data" -H "Authorization: {token_value}" -F "translation=@recognition.json" localhost:50000/translation > translation.json